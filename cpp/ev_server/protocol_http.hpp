#pragma once

#include "protocol.hpp"

namespace net {
    shared_ptr<Protocol> protocol_http( ev_tstamp timeout );
}

