/**
    Async http parser in a simple testable version made for async parsing,

    This is done in two steps, in order to keep parsing as simple as possible. The first
    step will make a buffer filled with the header part, by checking on the double \r\n
    marking, and then stop reading from the socket when this is detected. After this
    we will parse the header, make a Request and parse it to the router.

    All this are done using async communication controlled by libev, and when the router
    have found a handler, it will will be possible for this handler to either continue
    serving as an async handling (we still wait for coroutines), or change the mode
    to synchronous using things like threads.

    The main problem is to make a state machine where chunks of data can be send
    into, in any kind of size.
*/
#include <map>
#include <iostream>
#include <cstring>
#include <regex>
#include <experimental/random>

using namespace std;

const char* white_space = " \t\n\r\f\v";

// trim from end of string (right)
string rtrim(const string& s)
{
    return s.substr(s.find_last_not_of(white_space) + 1);
}

// trim from beginning of string (left)
string ltrim(const string& s)
{
    return s.substr(0, s.find_first_not_of(white_space));
}

// trim from both ends of string (left & right)
string trim(const string& s)
{
    return ltrim(rtrim(s));
}

// method uri http/version
static regex http_header("([[:alpha:]]+) (.+) HTTP/([[:digit:].]+)");

class HttpParser {
    string _head_buf;
    string _method,
        _uri,
        _http_ver;
    map<string,string> _header_in,
        _header_out;
    bool _gotit;
public:
    HttpParser() : _gotit( false ) {}

    /*
        Return the number of bytes consumed
    */
    size_t inject( const char *buf, size_t size ) {
        if( _gotit || size < 1 )
            return 0;

        size_t old_size = _head_buf.size();
        _head_buf.append( buf, size );
        size_t head_end = _head_buf.rfind( "\r\n\r\n" );

        if( head_end == string::npos )
            return size;

        _gotit = true;

        _head_buf.resize( head_end + 4 );
        parse_header(_head_buf);

        return (head_end + 4) - old_size;
    }

    bool has_header() const { return _gotit; }

    string get( const string &name ) const {return _header_in.find( name )->second;}
protected:
    string split_request( const string &line, string &method, string &uri ) {
        smatch match;
        string ver = "0.9";

        if( regex_search(line, match, http_header )) {
            if( match.size() == 4 ) {
                method = match.str( 1 );
                uri = match.str( 2 );
                ver = match.str( 3 );
            }
        }

        return ver;
    }

    void parse_header( const string &header ) {
        size_t pos = header.find_first_of("\r\n") + 2;
        _http_ver = split_request( header.substr( 0, pos ), _method, _uri );

        while( true ) {
            size_t i = header.find_first_of( "\r\n", pos ) + 2;

            if( i != string::npos ) {
                string l = header.substr( pos, i - pos );

                size_t p = l.find_first_of( ":" );
                if( p != string::npos ) {
                    string name = trim( l.substr( 0, p ));
                    string val = trim( l.substr( p + 1 ));

                    _header_in.insert( make_pair( name, val ));
                    pos = i;
                    continue;
                }
            }

            break;
        }
    }
};

const char *test_header = "GET /test.com HTTP/1.1\r\n"
   "test: value\r\n"
   "Content-Type: plain/text\r\n"
   "Content-Size: 4\r\n"
   "\r\nbody";

int main() {
    using namespace experimental;

    size_t len = strlen( test_header );
    long cnt = 0;

    while( true ) {
        size_t i = 0;

        HttpParser p;

        while( true ) {
            size_t n = randint( 1, (int)(len - i));

            size_t rn = p.inject( &test_header[ i ], n );

            i += rn;

            if( p.has_header() )
                break;
        }

        // cout << "first buffer char at pos " << i << " = " << (int)test_header[ i ] << endl;
        cout << "count " << cnt++ << endl;

        string content( &test_header[ i ], len - i );
        if( content != "body" || p.get( "test" ) != "value" ) {
            cerr << "bad content (" << content.size() << "): '" << content << "' ( " << i << " )" << endl;
            break;
        }
    }
}