#include "tools.hpp"

using namespace std;

// Header tools
const char* white_space = " \t\n\r\f\v";

// trim from end of string (right)
string_view tools::rtrim(string_view s)
{
    auto pos = s.find_last_not_of(white_space);

    return pos == string_view::npos ? s : s.substr(0, pos + 1);
}

// trim from beginning of string (left)
string_view tools::ltrim(string_view s)
{
    return s.empty() ? s : s.substr(s.find_first_not_of(white_space));
}

// trim from both ends of string (left & right)
string_view tools::trim(string_view s)
{
    return ltrim(rtrim(s));
}
