#include "request.hpp"
#include <experimental/socket>
#include <experimental/internet>

#pragma once

namespace acgi {
    using namespace std;
    namespace net = std::experimental::net;
    using net::ip::tcp;

    class RequestHttp : public Request {
        friend void async_read_http_request( tcp::socket &&socket, request_fn fn );
    protected:
        string _http_ver,
            _body_buf;
        size_t _transfered;
        shared_ptr<tcp::socket> _sock;

        string split_request( const string &line, string &method, string &uri );

        void header_send( tcp::socket &sock, bool close, complete_fn fn );

        size_t parse_header( string_view header );
    public:
        RequestHttp( shared_ptr<tcp::socket> sock ) : _sock( sock ) {}

        size_t length() const;

        void async_body_read( body_read_fn fn )

        void result_set( Result res, complete_fn fn );
    };

    // Parse the request, build a Request and send back answer when done all in async
    void async_read_http_request( tcp::socket &&socket, request_fn fn );
}