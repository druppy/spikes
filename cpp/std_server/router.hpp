/**
    This is a simple router, that takes a request object and delegate this to a handler function that match
    the requirements for a given request (reqex). The first matching handler will be called.

    When we find a match and a related handler, the router will look at the function registered and if this requires
    a session, it will ask the system to find or make one. As this could be done using complex database interaction
    we call this as an async function, to make sure that no matter how slow this is, it will not block.

    If the handler does not need a session, we don't want to pay the penalty and we therefor just call the matching
    handler.

    The idea is to have a loose coupling between the incoming requests, and the code that have an interest in
    handling the request.
*/
#include <functional>
#include <list>
#include <map>
#include <variant>
#include <any>
#include <regex>
#include <memory>
#include <string>
#include "request.hpp"

#pragma once

namespace acgi {
    using namespace std;

    class Session {
        map<string, any> _values;

        string _ident;
        time_t _touched;
    public:
        Session( const string_view id );

        string_view id_get() {return _ident;}

        bool has_a( const string_view name ) const;
        any get( const string_view name ) const;
        void set( const string_view name, any value );

        // Number of seconds old
        time_t age() const;

        // reset the session age by touching it
        void touch();
    };

    class SessionFactory {
        string _ident = "id";
        time_t _session_age = 60,
            _session_age_offset = 60; // seconds
        map<string, shared_ptr<Session>> _sessions;

        using session_handler = function<void( shared_ptr<Session> sess )>;

        SessionFactory();
        void cookie_set( shared_ptr<Request> req, string_view ident );
    public:
        static SessionFactory &inst();

        void ident_set( const string_view ident );

        void async_resolve( shared_ptr<Request> req, session_handler fn ); // XXX, This should be over writable in order to decouple later
    };

    // handler of request can be with or without session, depending on the function registered
    using result_handler = function<void()>;
    using request_handler = function<void(shared_ptr<Request> req)>;
    using request_session_handler = function<void(shared_ptr<Session> sess, shared_ptr<Request> req)>;
    using handler = variant<request_handler, request_session_handler>;

    class Router {
        list<pair<regex, handler>> _routes;
        request_handler _default_handler;

        Router() {}; // don't make an instance
    public:
        static Router &inst();

        Router& default_handler_set( request_handler fn );

        Router& append( string_view path, request_handler fn );
        Router& append( string_view path, request_session_handler fn );

        void dispatch( shared_ptr<Request> req, result_handler fn ) const;
    };
}