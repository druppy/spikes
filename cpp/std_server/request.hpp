/**
    Common request class that defines what kind of information a incoming request looks
    like regardless of protocol.
*/
#include <string>
#include <ostream>
#include <map>
#include <functional>
#include <memory>
#include <iostream>
#include <experimental/buffer>
#include "tools.hpp"
#pragma once

namespace acgi {
    using namespace std;
    namespace net = std::experimental::net;

    using body_read_fn = function<bool( const error_code &ec, net::const_buffer buf )>;
    using body_write_fn = function<bool( const error_code &ec, size_t len )>;
    using complete_fn = function<void(const error_code &ec)>;

    class Request {
        enum Result {
            HTTP_OK = 200,
            HTTP_NOT_FOUND = 404
        };
    protected:
        string _method,
            _uri;
        map<string,string> _header_in,
            _header_out;
    public:
        string_view uri() const {return _uri;}
        string_view method() const {return _method;}

        virtual size_t length() const = 0;

        string cookie_get( string_view name ) const;

        string get( string_view name ) const;
        void set( string_view name, string_view val );

        /**
            Reads chunk of body, if any data, will return end of body if no more is found

            the same function may be called more than ones, until ec is eof.
        */
        virtual void async_body_read( body_read_fn fn ) = 0;

        /**
            Write chunk of body, can only write the amount defined by content-length. If the
            buffer does not contain the full body of data, it is possible to call this function
            again until the full length have been send, as defined in body_set

            Note that the body will not be send until the result_set function have been
            called.

            This could also be part of a streaming connection, and in that case the callback
            function will keep calling until socket have been closed and ec return eof.
        */
        void async_body_write( net::mutable_buffer &buf, body_write_fn fn );

        /**
            If data need to be send back, this will define the mimetype and the length,
            use async_body_write for actual data.

            The data will first be send after result_set have been called.
        */
        void body_set( size_t length, string_view mimetype = "text/plain" );

        // set return code, and start send whatever body that needs to be set
        virtual void result_set( Result res, complete_fn fn ) = 0;

        void dump( ostream &os ) const;
    };

    inline ostream &operator<<(ostream &os, const Request &h ) {h.dump( os ); return os;}

    using request_fn = function<void(shared_ptr<Request> req)>;

    // composed function for body writing into a dynamic buffer (be careful if the body is huge)
    void async_body_read( Request &req, net::dynamic_buffer &body, complete_fn fn );
}