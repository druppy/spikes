## fuwu (服务) web service core (uCGI ???)

Basic web service handler, that will take care of all things needed to write large backend service in C++-17 style.

While making this core, I have the following goals :

 * Use C++14, and prefer standards over anything else.
   - thread handling
   - variant (C++-17)
 * Don't reinvent more than needed, so
   * I use libpqxx as database handler (I only use Psql)
   * use libev for all internal network
 * offload a many things as possible, to make performance nice and to prevent
   reinventing wheels again.

The idea of using C++ is of cause speed, but another thing is the language's strict typing and generic template system, that really is a big help, together with a deterministic memory model.

# Internal elements

The server are made of 4 layers, handling there own responsibilities 

1. Protocol

  This will take care of the listening to any communication and all initial 
  protocol handling, in order to extract info needed for routing.

  The protocol handler will listen to one or more channels and parse the request until the end of the header. It will then pass along this info in the form as a Request instance, carefully making sure the related socket are at the position of the related body, if it is found.

  When the request is ready, it will end up in a queue for later processing as we don't want to protocol and listener to use to much time on things like session lookup in databases and other strange things.

  The reader thread of this request queue will use time to see if any of the related request have a related session, and if it has one it will look it up and bind it to the request. If no cookie are found it will relate a new session to the request.

  When this is done, the request (or session) will be passed to the router.

2. Routing

  As all protocols handled by fuwu is based on [HTTP] (or [SCGI]), we will assume that we always know the URI, method and content type of the incoming request, so we make a router that can dispatch each incoming request to the proper handler using at least these simple parts of the request header.

3. Event handling

  When handling things like events (like SSE), to several clients it is important to keep this service async and as a basic kind of service.

  Therefor will the event system stay an async service, but will at the same time be an internal service too, as this makes it all the more useful.

4. Services

  Services are many things, but we try to keep these to a minimum to make interfacing as simple as possible. Each service may overlap in functionality but this makes it possible to do the same in more than one way.

  * JsonRPC 2.0

    This is a very traditional RPC service using JSON and using the idea of remote function the main paradigm. 

    As an addition to this, it is possible to send a notice, that is a function with no return value, and sending more than one function in a single batch, which will be able to keep the amount of requests down and may also pack all actions into one atomic action.

    Last but not least it may be possible to use results from one action in a batch as argument to the next, in to be able to make chains of depending actions like creation of depending entities.

  * RESTFul JSON

    The RESTFul interface is the [HTTP] version of [CRUD], that that has all the benefits from the [HTTP] protocol, and it is excellent for list searching as it can be made possible to work together with browser caching, at many layers. It is possible to both delete, create and update using this system but each action are done in a request and it is not possible to batch these or make them atomic.

    - GET ../id -> do_get
    - GET ..?   -> do_list
    - POST      -> do_create
    - PUT       -> do_update
    - DELETE    -> do_delete  

    All REST calls gets an Session ref as first argument.

    The do_list function may also provide cache setup and etag handling, as this may be a relevant place to optimize. If etaging are used we need to maintain a version id of a kind, each time data is changed on the entity.

    do_list will accept HTTP range header elements, in order to support paging, and these values will also be part or the caching.

    As a side effect RESTFul interface uses an internal model called entity, and all services has an access to, and that hold meta data about the entity that allow for data validation, both for columns and search arguments.

  * [GraphQL]

    This combines the compact structure possible in batch rpc and the generic nature of the RESTFul entity model.

    For mobile applications this is a really nice interface as the client can specify what exactly it needs and the relations in the data, and the server will provide this and nothing more.

    This interface uses the same entity model as RESTful, but depends more on the meta data defining the relation between entities.

# Components

Inside the program we have some central and important components or classes the will be a good idea to understand in order to use and work with the system.

* Request

  Each time a client contact out service we create a request and this request will contain all header information given by the caller, regardless of the protocol or port. Re request also holds a socket id that points the the first byte in the data, if any is given.

* Session

  This defines a single client, so that it is possible for our service to references to the same client on each request from this. This will be used to identify a user on the system, and control authentication and other things.

  All the logic about getting and setting cookies on the client are handled by the core logic and the rest of the system dont need to worry about it.

  * Session values

    A session can hold a set of values that will exist as long as the session
    exists, when the session no longer exist these values are lost. These are
    typically used for client states like login.

  * Config values

    There are config set for the server to startup (in uwsgi), and then there are config value only relevant for the client system, or the logic of the app.

    These can be stored in config, and can either be system wise or user specific.
    There the user specific takes precedence over system wise.

* Entity

  An entity defines a set of known columns and a set of known arguments.

  It also defines if a column is sortable, readonly or mandatory for if creating new entity instances.

  An entity are accessible from all the services, and is by itself not service aware. 

  Meta data on an entity contains :

  - columen names, and types (json types), order, readonly, mandatory
  - search args types
  - relation to other entities 

  It looks much like a database table, and it could easily be exactly this, but it may be anything like both a combination of tables or just some in memory things.

* Method and notices

  Is a JsonRPC method call, internaly this gets a Session ref as first argument and what ever argument it needs a the rest.

  A method require a return value, and a notice does not.

  It is even possible to make C++ understand a function footprint directly
  while compiling, in order to be able to give more precise error handling
  and introspection.

  A client may call a single method per request or a batch, to reduce the network overhead.

* Event

  Inside the backend we have an event system that makes it possible to make loose kopling between elements in the system. This system may be be user for a offloaded user event system.

* SMD

  [SMD spec]

  To be able as the service from a client what the server is able to do, this  is service provides it by asking all emtity and methods about there meta data it may compile this kind of list, and the client can then validate and test its interface.

[SMD spec 2]

[CRUD]: https://en.wikipedia.org/wiki/Create,_read,_update_and_delete
[SMD spec]: http://dojotoolkit.org/reference-guide/1.10/dojox/rpc/smd.html
[SMD spec 2]: http://www.jsonrpc.org/specification
[HTTP]: https://www.ietf.org/rfc/rfc2616.txt
[GraphQL]: http://graphql.org
[SCGI]: https://python.ca/scgi/protocol.txt