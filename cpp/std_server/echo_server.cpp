#include <experimental/executor>
#include <experimental/io_context>
#include <experimental/timer>
#include <experimental/buffer>
#include <experimental/socket>
#include <experimental/internet>
#include <iostream>
#include <memory>
#include "tools.hpp"

using namespace tools;
using namespace std;
namespace net = std::experimental::net;
using net::ip::tcp;

class ConnectionHandler : public enable_shared_from_this<ConnectionHandler>
{
    tcp::socket _sock;
    string _sbuf;

public:
    ConnectionHandler( tcp::socket &&sock ) : _sock( move( sock )) {
        cout << __PRETTY_FUNCTION__ << endl;
    }

    ~ConnectionHandler() {
        cout << __PRETTY_FUNCTION__ << endl;
    }

    void communicate() {
        shared_ptr<ConnectionHandler> ref( shared_from_this());

        _sock.async_send( net::buffer( "Hello" ), [this, ref]( const error_code &ec, auto len ) -> void {
            net::async_read_until(_sock, net::dynamic_buffer(_sbuf), '\n',
                [this, ref]( const error_code &ec, auto len ) -> void {
                    if( !ec ) {
                        trim( _sbuf );

                        cout << "client say '" << string_view((const char *)_sbuf.data(), _sbuf.size()) << "'" << endl;
                        if( _sbuf != "quit" ) {
                            _sbuf.clear();
                            communicate();
                        }
                    } else
                        cout << "error " << ec.message() << endl;
                });
        });
    }
};

class AcceptHandler {
    net::io_context &_ctx;
    tcp::resolver _resolver;
    vector<shared_ptr<tcp::acceptor>> _accs;

public:
    AcceptHandler( net::io_context &ctx ) : _ctx( ctx ), _resolver( ctx ) {}

    void connect( const string &host, const string &service ) {
        cout << "Resolve " << host << endl;

        _resolver.async_resolve( host, service , [this, host](auto ec, tcp::resolver::results_type results ) -> void {
            if( !ec ) {
                cout << "host " << host << " resolves to " << results.size() << endl;

                for( auto end_point: results ) {
                    auto acc = make_shared<tcp::acceptor>( _ctx );

                    acc->open( tcp::v4() );
                    acc->set_option(tcp::acceptor::reuse_address(true));
                    acc->bind( end_point );
                    acc->listen();

                    _accs.push_back( acc );

                    acc->async_accept([this]( const std::error_code &ec, tcp::socket peer ) -> void {
                        cout << "accept " << peer.remote_endpoint() << " from user " << endl;
                        accept( ec, peer );
                    });
                }
            } else {
                cerr << "Error resolving " << host << ": " << ec << endl;
            }
        });
    }

protected:
    void accept( const std::error_code &ec, tcp::socket &peer ) {
        auto c = make_shared<ConnectionHandler>( move( peer ));

        c->communicate();
    }
};

int main( int argc, char *argv[] )
{
    net::io_context ctx;

    AcceptHandler hndl( ctx );

    hndl.connect( "localhost", "9000" );

    ctx.run();
}