#include "request.hpp"

namespace acgi {
    using namespace std;

    string Request::cookie_get( string_view name ) const {
        for( auto pair: tools::split<string>( string( get( "Cookie" )), ";" )) {
            auto parts = tools::split<string>( pair, "=" );

            if( parts.size() == 2 && parts[0] == name )
                return parts[ 1 ];
        }

        return "";
    }

    string Request::get( string_view name ) const {
        auto i = _header_in.find( string( name ));
        return i == _header_in.end() ? "" : i->second;
    }

    void Request::set( string_view name, string_view val ) {
        _header_out[ string( name ) ] = string( val );
    }

    void Request::dump( ostream &os ) const {
        os << "URI: " << _uri << endl;
        os << "Method: " << _method << endl;

        for( auto h: _header_in )
            os << h.first << " = " << h.second << endl;
    }
}