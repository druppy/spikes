#include "protocol_http.hpp"

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sstream>
#include <memory>
#include <algorithm>
#include <map>
#include <iostream>
#include <cstring>
#include <regex>
#include <set>

namespace net {
    using namespace std;

    string rtrim( const string &str )
    {
        auto iter = str.rbegin();

        while( iter != str.rend() && isspace( *iter ))
                iter++;

        return str.substr( 0, str.length() - (iter - str.rbegin()) );
    }

    string ltrim( const string &str )
    {
        auto iter = str.begin();
        while( iter != str.end() && isspace( *iter ))
                iter++;

        return string( iter, str.end());
    }

    string trim( const string &str )
    {
        return rtrim( ltrim( str ));
    }

    // method uri http/version
    static regex http_header("([[:alpha:]]+) (.+) HTTP/([[:digit:].]+)");

    class HTTPRequest : public Request {
        string _head_buf;
        bool _gotit;
    public:
        HTTPRequest() : Request(), _gotit( false ) {
            _head_buf.reserve( 2048 );
        }
        HTTPRequest( shared_ptr<Protocol> prod, int sock ) : Request( prod, sock ), _gotit( false ) {
            _head_buf.reserve( 2048 );
        }

        /*
            Return the number of bytes consumed
        */
        size_t inject( const char *buf, size_t size ) {
            if( _gotit || size < 1 )
              return 0;

            size_t old_size = _head_buf.size();
            _head_buf.append( buf, size );
            size_t head_end = _head_buf.find( "\r\n\r\n" );

            if( head_end == string::npos )
                return size;

            _gotit = true;

            _head_buf.resize( head_end + 4 );
            parse_header( _head_buf );

            return ( head_end + 4 ) - old_size;
        }

        bool has_header() const { return _gotit; }

        size_t header_buf_size() const {return _head_buf.size();}

        string get( const string &name ) const {
            return _headers_in.find( name )->second;
        }

        /**
            Release the request and socket from protocol handler

            Future protocol handing are then done by the one taking over the
            request. Even if this request goes out of scope the socket is still
            not closed.

            This is mostly done to let sse take over an request, maybe not the most beautiful
            of solutions, but it will work for now.
        */
        void takeover() override;
    protected:
        string split_request( const string &line, string &method, string &uri ) {
            smatch match;
            string ver = "0.9";

            if( regex_search(line, match, http_header )) {
                if( match.size() == 4 ) {
                    method = match.str( 1 );
                    uri = match.str( 2 );
                    ver = match.str( 3 );
                }
            }

            return ver;
        }

        void parse_header( const string &header ) {
            size_t pos = header.find_first_of("\r\n") + 2;
            _version = split_request( header.substr( 0, pos ), _method, _uri );

            while( true ) {
                size_t i = header.find_first_of( "\r\n", pos ) + 2;

                if( i != string::npos ) {
                    string l = header.substr( pos, i - pos );

                    size_t p = l.find_first_of( ":" );
                    if( p != string::npos ) {
                        string name = header_normalize(trim( l.substr( 0, p )));
                        string val = trim( l.substr( p + 1 ));

                        _headers_in.insert( make_pair( name, val ));
                        pos = i;
                        continue;
                    }
                }

                break;
            }
        }
    };

    size_t _conn_count = 0;

    // HTTP protocol connection handler, that takes care of passing http for a single connection to a client
    class ProtocolHttp : public Protocol, public enable_shared_from_this<ProtocolHttp> {
        shared_ptr<HTTPRequest> _req;
        int _sock;
        int _errno;
        ev_tstamp _timeout;
        bool _keep_alive;
        bool _inited;
        enum State {
            stateError,
            stateReq,
            stateReqParser,
            stateRespHeader,
            stateRespBody,
            stateRespEnd
        } _state;
        ev::io _io_write;
        ev::io _io_read;
        ev::async _async_finish;
        ev::timer _timer;
        ev::timer _reread_timer;

        StringPipe _header; // output stream
        shared_ptr<Pipe> _body;

        bool write_some( Pipe &pipe );

        void writer();
        void reader();

        void ev_read( ev::io &w, int revents );
        void ev_write( ev::io &w, int revents );
        void ev_finish( ev::async &w, int revents );
        void ev_timeout( ev::timer &w, int revents );
        void ev_reread_timeout( ev::timer &w, int revents );

        void terminate();
        void reset();
        void close();

        ProtocolHttp( const ProtocolHttp & ) = delete;
        ProtocolHttp &operator = (const ProtocolHttp &) = delete;
    public:
        ProtocolHttp();
        ProtocolHttp( request_handler_t fn );
        ~ProtocolHttp();

        void setup( ev_tstamp timeout ); // use this as shared ptr does not work in constructor until C++17 ?
        int error_last() const { return _errno;}

        void handle_start( int sock ) override;
        void handle_finish( Request *req, HTTPStatus status ) override;

        void takeover() override;
    };

    /**
        The protocol handler need to be deleted from a place that is not a direct
        call from one of its functions. So we simply register the protocol here, and
        on the next idle loop we will release it, and the destruction of the protocol
        may occur here.
    */
    class IdleGc {
        list<shared_ptr<ProtocolHttp>> _list;
        ev::idle _idle;
        ev::timer _timed;
    public:
        IdleGc() {
            _idle.set<IdleGc, &IdleGc::io_idle>( this );
            _timed.set<IdleGc, &IdleGc::io_timed>( this );

            _timed.start( 1, 1 );
        }

        void add( shared_ptr<ProtocolHttp> prot ) {
            _list.push_back(prot);

            _timed.start( 1, 1 );
            //if( !_idle.is_active())
            //    _idle.start();
        }

        bool empty() const {
            return _list.empty();
        }

        shared_ptr<ProtocolHttp> pop() {
            auto p = _list.back();
            _list.pop_back();
            return p;
        }

        void io_timed( ev::timer &t, int revents ) {
            _list.clear();
        }

        void io_idle( ev::idle &idle, int revents ) {
            if( _list.empty())
                _idle.stop();
            else
                _list.pop_back();
        }
    };

    class PipeReader {
        char _buffer[ BUFSIZ ];
        int _index;
        int _length; // -1 = no data
        shared_ptr<Pipe> _pipe;
    public:
        PipeReader( shared_ptr<Pipe> pipe ) : _index( -1 ), _length( 0 ) {
            _pipe = pipe;
        }

        int read( char *buf, int len ) {
            if( _index != -1 ) {
                int n = _pipe.read( _buffer, BUFSIZ );
                if( n > 0 ) {
                    _index = 0;
                    _length = n;
                } else
                    return n;
            }

            int n = _length - _index;
            memcpy( buf, _buffer[ _index ], )
        }
    };

    IdleGc idle;

    ProtocolHttp::ProtocolHttp() : _inited( false ), _state( stateReq )
    {
        ++_conn_count;
    }

    ProtocolHttp::ProtocolHttp( request_handler_t fn ) : Protocol( fn ), _inited( false ), _state( stateReq )
    {
        ++_conn_count;
    }

    ProtocolHttp::~ProtocolHttp()
    {
        if( _state == stateError )
            clog << this << ":" << _sock << " error on connection " << strerror( _errno ) << endl;

        //if( _state != stateRespEnd && !_keep_alive )
        //    clog << this << ":" << _sock << " closed in state " << _state << " conn count is " << _conn_count << endl;

        close();

        --_conn_count;
    }

    void ProtocolHttp::close()
    {
        if( _sock != -1 ) {
            // clog << this << ":" << _sock << " closing handle" << endl;

            if( ::close( _sock ) == -1 )
                clog << this << ":" << _sock << " close handle error " << strerror(errno) << endl;

            _sock = -1;
        }
    }

    // call this after make_shared as shared_from_this does not work in the constructor !!!
    void ProtocolHttp::setup( ev_tstamp timeout ) {
        if( !_inited ) {
            _io_read.set<ProtocolHttp, &ProtocolHttp::ev_read>( this );
            _io_write.set<ProtocolHttp, &ProtocolHttp::ev_write>( this );
            _async_finish.set<ProtocolHttp, &ProtocolHttp::ev_finish>( this );
            _timer.set<ProtocolHttp, &ProtocolHttp::ev_timeout>( this );
            _reread_timer.set<ProtocolHttp, &ProtocolHttp::ev_reread_timeout>( this );
            _inited = true;
        }
        _timeout = timeout;
        _keep_alive = false;
        _sock = -1;
        _errno = 0;
        _body.reset();
    }

    void ProtocolHttp::takeover() {
        _sock = -1;
        terminate();
    }

    // Called when we got a connection, but no data was to read on this
    void ProtocolHttp::ev_reread_timeout( ev::timer &w, int revents )
    {
        clog << this << ":" << _sock << " reread timeout in state " << _state << " conn count is " << _conn_count << endl;

        reader();
    }

    void ProtocolHttp::reader()
    {
        /*if( _state != stateReq ) {
            clog << this << ":" << _sock << " reads in wrong state " << _state << " on this " << this << endl;
            return;
        }*/

        char buf[ BUFSIZ ];

        // Collect header data but stop reading after the header termination
        ssize_t n = recv( _sock, &buf, sizeof( buf ), MSG_PEEK );

        if( n > 0 ) {
            ssize_t n2 = _req->inject( buf, n );

            if( n2 > 0 ) {
                if( recv( _sock, &buf, n2, 0 ) == -1 ) {
                    _errno = errno;
                    _state = stateError;
                    terminate();
                }
            } else
                clog << this << ":" << _sock << "inject 0 bytes in request" << endl;

            _reread_timer.stop();

            // clog << this << ":" << _sock << " reads " << n << " data in state " << _state << endl;

            if( _req->has_header()) {
                _state = stateReqParser;
                _io_read.stop();
                _io_write.stop();
                _timer.stop();
                _async_finish.start(); // be ready for an async finish

                _fn( _req );
            }
        } else {
            if( n < 0 ) {
                if(errno == EAGAIN || errno == EWOULDBLOCK) {
                    if( !_io_read.is_active())
                        _io_read.start(_sock, ev::READ);

                    _reread_timer.start(10, 10);
                } else {
                    _errno = errno;
                    _state = stateError;
                    terminate();
                }
            }

            if( n == 0 ) {
                /*clog << this << ":" << _sock << " recv gets 0 bytes, terminating connection in state " << _state
                    << " req header buffer size " << (_req ? _req->header_buf_size() : -1) << endl;*/

                terminate();
            }
        }
    }

    void ProtocolHttp::ev_read( ev::io &w, int revents )
    {
        if( _sock == -1 ) {
            terminate();
            return;
        }

        if( ! _req ) {
            clog << this << ":" << _sock << " missing request ref " << endl;
            return;
        }

        if(ev::ERROR & revents) {
            clog << this << ":" << _sock << " got invalid read event" << endl;
            return;
        }

        if( ev::READ & revents )
            reader();
    }

    /**
        Read some data from the pipe and write as much of it as possible to the socket

        It write to the standard socket and reads from the socket given

        @return true when more need to be written, else false to stop
    */
    bool ProtocolHttp::write_some( Pipe &pipe ) {
        char buf[ BUFSIZ ];
        ssize_t s = sizeof( buf );

        s = pipe.read( buf, s );

        if( s == 0 ) { // no data at the moment, try again later
            _io_write.stop();
            return false;
        }

        if( s > 0 ) {
            ssize_t n = ::send( _sock, buf, s, MSG_NOSIGNAL);

            if( !_io_write.is_active())
                _io_write.start( _sock, ev::WRITE );

            if( n < 0 ) {
                if(errno == EAGAIN || errno == EWOULDBLOCK) {
                    pipe.unread(buf, s); // unread the pipe as we could not send anyway
                    // clog << this << ":" << _sock << " put data back in pipe as of EAGAIN" << endl;
                    return true;
                } else {
                    _errno = errno;
                    _state = stateError;
                    terminate();
                    return false;
                }
            }

            assert( n != 0 );

            if( s > n ) {
                // clog << this << ":" << _sock << " not all of the buffer was send" << endl;
                pipe.unread(&buf[ n - 1 ], s - n);
            }

            // clog << this << ":" << _sock << " send " << n << " data to client in state " << _state << endl;
            return true;
        }

        return false;
    }

    // common function for writing data on async events
    void ProtocolHttp::writer() {
        switch( _state ) {
            case stateRespHeader:
                // No on_ready are needed on StringPipe, so we omit it here
                if( !_header.done())
                    if( write_some( _header ))
                        break;

                _state = stateRespBody;
                break;

            case stateRespBody:
                if( _body ) {
                    // If this is the first time we meet this pipe, make sure to register on ready
                    if( _body->on_ready_size() == 0 )
                        _body->on_ready([this]() {
                            // _io_write.start( _sock, ev::WRITE );
                            writer();
                        });

                    if( !(_body->done()))
                        if( write_some( *_body ))
                            break;
                }

                _state = stateRespEnd;
                break;

            case stateRespEnd:
                reset();
                // clog << this << ":" << _sock << " resp end" << endl;
                break;

            default:
                clog << this << ":" << _sock << " write in wrong state " << _state << endl;
                break;
        }
    }

    // Callback func for libev
    void ProtocolHttp::ev_write( ev::io &w, int revents )
    {
        if(ev::ERROR & revents) {
            clog << this << ":" << _sock << " got invalid write event" << endl;
            return;
        }

        if (revents & ev::WRITE)
            writer();
    }

    void ProtocolHttp::ev_finish( ev::async &w, int revents ) {
        if(ev::ERROR & revents) {
            clog << this << ":" << _sock << " got invalid write event" << endl;
            return;
        }

        _timer.stop();
        writer();
    }

    void ProtocolHttp::ev_timeout( ev::timer &w, int revents ) {
        clog << this << ":" << _sock << " timeout connection in state " << _state << endl;

        if( _req && _state == stateReq )
            handle_finish(_req.get(), HTTP_REQUEST_TIMEOUT );
        else
            terminate();
    }

    /**
        To close the connection we need to stop all events, remove our lambda relation and
        and make sure we only deref from an handler outside this class.

        The idle gc will deref the instance next time we have no events in the system.
    */
    void ProtocolHttp::terminate() {

        _io_read.stop();
        _io_write.stop();
        _timer.stop();
        _reread_timer.stop();
        _async_finish.stop();

        idle.add(shared_from_this()); // Make sure last ref is released on next idle loop

        _fn = nullptr; // Release the callback function (until we make 1.1 support)
        close();
    }

    /**
        To continue to use this connection, we need to reset state, make a new request object
        reset timer, and then start reading as we got a new request.
    */
    void ProtocolHttp::reset()
    {
        _io_write.stop();
        _timer.stop();
        _reread_timer.stop();
        _async_finish.stop();

        if( !_io_read.is_active())
            _io_read.start(_sock, ev::READ);

        if( _keep_alive ) {
            _req = make_shared<HTTPRequest>( shared_from_this(), _sock );
            _body.reset();
            _timer.start( _timeout, 0 );
            _state = stateReq;
            reader();
        } else
            terminate();
    }

    void ProtocolHttp::handle_start( int sock )
    {
        if( _sock != -1 )
            throw invalid_argument( "missing socket on protocol" );

        // clog << this << ":" << sock << " open socket, conn count is " << _conn_count << endl;

        _sock = sock;
        _state = stateReq;
        _req = make_shared<HTTPRequest>( shared_from_this(), _sock );

        _timer.start( _timeout, 0 );

        reader();
    }

    // Finish the request by sending back header and data ... asynchronously
    void ProtocolHttp::handle_finish( Request *req, HTTPStatus status )
    {
        if( _sock == -1 )
            throw invalid_argument( "missing socket on protocol" );

        if( _state != stateReqParser ) {
            clog << this << ":" << _sock << " handle finish in wrong state " << _state << endl;
            terminate();
        }

        _keep_alive = _req->version() == "1.1";
        if( _req->version() == "1.0" ) { //
            if( req->has_a( "connection" ) && string_tolower( req->get( "connection" )) == "keep-alive" )
                    _keep_alive = true;
        }

        if( status == HTTP_MOVED_PERMANENTLY )        // IE need this in a none persistent connection, so we stop here !
            _keep_alive = false;

        if( _req->version() == "1.1" || _keep_alive )
            req->append( "Connection", _keep_alive ? "keep-alive" : "close" ); // no keep alive !!!

        stringstream os;
        req->prepare_headers( os, status );
        _header.set( os.str());
        _body = req->body_pipe();
        _state = stateRespHeader;

        socket_nonblock_set( _sock );

        _async_finish.send(); // writer(); 
        // we may be called from another thread here !!!
    }

    void HTTPRequest::takeover() {
        auto p = _prot.lock();
        p->takeover();
    }

    shared_ptr<Protocol> protocol_http( ev_tstamp client_timeout )
    {
        auto p = idle.empty() > 0 ? make_shared<ProtocolHttp>() : idle.pop();

        p->setup( client_timeout );

        return p;
    }
}