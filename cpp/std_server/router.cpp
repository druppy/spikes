#include "router.hpp"
#include <random>
#include <regex>
#include <iostream>

namespace acgi {
    using namespace std;

    Session::Session( const string_view id ) : _ident( id ), _touched(time(nullptr)) {}

    bool Session::has_a( const string_view name ) const {
        return _values.count( string( name )) > 0;
    }

    any Session::get( string_view name ) const {
        auto i = _values.find( string( name ));
        if( i != _values.end() )
            return i->second;

        throw invalid_argument("unknown session value " + string( name ));
    }

    void Session::set( string_view name, any value ) {
        _values[ string( name )] = value;
    }

    time_t Session::age() const {
        return time(nullptr) - _touched;
    }

    void Session::touch() {
        _touched = time(nullptr);
    }

    random_device rd;

    static string make_ident( size_t size ) {
        string ident;
        uniform_int_distribution<> dist( (int)'a', (int)'z' );

        for( size_t i = 0; i != size; i++ )
            ident += (char)dist(rd);

        return ident;
    }

    SessionFactory::SessionFactory() : _ident( "id" ) {}

    SessionFactory &SessionFactory::inst() {
        static SessionFactory *factory = nullptr;

        if( factory == nullptr )
            factory = new SessionFactory();

        return *factory;
    }

    void SessionFactory::ident_set( const string_view ident )
    {
        _ident = ident;
    }

    void SessionFactory::cookie_set( shared_ptr<Request> req, string_view ident )
    {
        stringstream os;
        timespec ts;
        timespec_get(&ts, TIME_UTC);
        ts.tv_sec += 60 * (_session_age + _session_age_offset);
        char buf[100];
        std::strftime(buf, sizeof buf, " ;Expires=%a, %e %b %Y %T GMT", std::gmtime(&ts.tv_sec));
        os << _ident << "=" << ident << buf;

        req->set( "Set-Cookie", os.str() );
    }

    void SessionFactory::async_resolve( shared_ptr<Request> req, session_handler fn )
    {
        string ident( req->cookie_get( _ident )); // XXX We need the cookie parser here !

        auto i = _sessions.find( ident );
        if( i != _sessions.end()) {
            if( i->second->age() > _session_age )
                cookie_set( req, ident );

            i->second->touch();
            fn( i->second );
        } else {
            string new_ident( make_ident( 20 ));
            auto sess = make_shared<Session>(new_ident);

            cookie_set( req, new_ident );

            _sessions[ new_ident ] = sess;
            fn( sess );
        }
    }

    // The router
    Router& Router::inst() {
        static Router *inst = nullptr;

        if( inst == nullptr )
            inst = new Router();

        return *inst;
    }

    Router& Router::default_handler_set( request_handler fn ) {
        _default_handler = fn;
        return *this;
    }

    Router& Router::append( string_view path, request_handler fn )
    {
        _routes.push_back( make_pair( regex( string( path )), fn ));
        return *this;
    }

    Router &Router::append( string_view path, request_session_handler fn )
    {
        _routes.push_back( make_pair( regex( string( path )), fn ));
        return *this;
    }

    void Router::dispatch( shared_ptr<Request> req, result_handler fn ) const {
        string uri( req->uri());

        for( auto route: _routes ) {
            if( regex_search(uri, route.first )) {
                if( holds_alternative<request_handler>( route.second )) {
                    auto fn = get<request_handler>( route.second );

                    fn( req );
                } else if( holds_alternative<request_session_handler>( route.second )) {
                    SessionFactory::inst().async_resolve(req, [req, route, this]( shared_ptr<Session> sess ) {
                        auto fn = get<request_session_handler>( route.second );

                        fn( sess, req );
                    });
                }
            } else
                if( _default_handler )
                    _default_handler( req );
        }
    }
}