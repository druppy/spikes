# Standart async server spike

This server model are based on C++17 and the [Networking TS], that really derive from [asio]. the reason that this many be a good choice is the fact that [asio] has been around for quite a long time and is both really solid and also enforces really good structure, for a scalable base system. 

The idea of using [Networking TS] is the use a more standard and modern library, that more people understands and that is well documented and then try to use that structure for the rest of the server. The downside could be lack of flexibility .. that is the reason for this spike :-)

[asio]: https://think-async.com
[Networking TS]: https://github.com/chriskohlhoff/networking-ts-impl

## Echo server

The first spike here is a simple echo server that simply echo all lines send to it, and it shows how to setup simple asynchronous servers, using networking TS and C++ 17.

## Async CGI

The second server is a CGI like service, that can handle multi connection and multi protocol, simple routing and session control, all done using async Networking TS. 

This will be the fundamental structure for more complex C++ CGI service handler. It will be easy to add new request handlers, and session handling can easily be persisted to an external storage (like redis). 

The request handler will not know anything about protocol handling, at the low level but will only use http headers and return values. 

The idea is to keep using the async model all the way, where the handing of both requests and the protocol handling is keept in the same structure as NetTS (ASIO).