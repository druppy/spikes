#ifndef __SOCK_ISTREAM__
#define __SOCK_ISTREAM__

#include <istream>

std::istream *make_sock_istream( int sock );

#endif