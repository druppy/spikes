#include "request.hpp"

#pragma once

#include "request.hpp"
#include <experimental/socket>
#include <experimental/internet>

#pragma once

namespace acgi {
    using namespace std;
    namespace net = std::experimental::net;
    using net::ip::tcp;

    class RequestScgi : public Request {
    protected:
        string split_request( const string &line, string &method, string &uri );

    public:
        void header_send( tcp::socket &sock, bool close, function<void( const error_code &ec )> fn );

        void parse_header( string_view header );
    };

    // Parse the request, build a Request and send back answer when done all in async
    void async_read_scgi_request( tcp::socket &&socket, request_fn fn );
}