#include "sock_istream.hpp"
#include <streambuf>
#include <vector>
#include <sys/types.h>
#include <sys/socket.h>
#include <cstring>

using namespace std;

// socket rdbuf for C++ streams reading from uwsgi socket
class sock_rdbuf : public streambuf
{
    public:
        explicit sock_rdbuf(int sock);

        ssize_t total() const {return _total;}
    private:
        // overrides base class underflow()
        streambuf::int_type underflow();

        // copy ctor and assignment not implemented;
        // copying not allowed
        sock_rdbuf(const sock_rdbuf &);
        sock_rdbuf &operator= (const sock_rdbuf &);
    private:
        int _sock;
        ssize_t _total;
        const size_t _put_back;
        vector<char> _buffer;
};

sock_rdbuf::sock_rdbuf( int sock )
    : _sock( sock ), _put_back( 1 ), _buffer( 100 + _put_back )
{
    _total = 0;
    char *end = &_buffer.front() + _buffer.size();
    setg(end, end, end);
}

streambuf::int_type sock_rdbuf::underflow()
{
    if (gptr() < egptr()) // buffer not exhausted
        return traits_type::to_int_type(*gptr());

    char *base = &_buffer.front();
    char *start = base;

    if (eback() == base) // true when this isn't the first fill
    {
        // Make arrangements for putback characters
        memmove(base, egptr() - _put_back, _put_back);
        start += _put_back;
    }

    // start is now the start of the buffer, proper.
    // Read from req socket in to the provided buffer
    ssize_t n = recv( _sock, start, _buffer.size() - (start - base), 0);
    if (n == -1)
        return traits_type::eof();

    _total += n;

    // Set buffer pointers
    setg(base, start, start + n);

    return traits_type::to_int_type(*gptr());
}

// Create a read stream from a socket id
istream *make_sock_istream( int sock )
{
    auto rdbuf = new sock_rdbuf( sock );

    return new istream( rdbuf );
}