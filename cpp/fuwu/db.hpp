/**
    A minimal SQL database API to make future integrations more simple

    The sql statements are possible to interpulate Values into using the
    %(name) syntax. The name will be requested using the resolve function
    and the returned value will be inserted as prober SQL escaped values.
*/

#include <string>
#include <stdexcept>
#include <memory>

#include "../value/value.hpp"

namespace DB {
    using namespace std;
    using namespace core;

    class Error : public exception {
        string _msg;
        string _stmt;
    public:
        Error( const string &msg, const string &stmt = "") : _msg( msg ), _stmt( stmt ) {}

        string message() const { return _msg; }
        string stmt() const { return _stmt; }

        const char *what() const throw() {
            return _msg.c_str();
        }
    };

    class Result {
    public:
        virtual bool next() = 0;

        virtual size_t columns() const = 0;
        virtual long long rows() const = 0;

        virtual bool is_null( const string &field_name ) const = 0;
        virtual Value get( const string &field_name ) const = 0;
    };

    typedef function<Value(const string&)> resolve_fn;

    class Conn {
    public:
        virtual bool begin() = 0;
        virtual bool commit() = 0;
        virtual bool rollback() = 0;

        virtual bool savepoint( const string &name ) = 0;
        virtual bool savepoint_release( const string &name ) = 0;
        virtual bool savepoint_roleback( const string &name ) = 0;

        virtual string escape( const Value &val ) const = 0;

        virtual shared_ptr<Result> query( const string &stmt, resolve_fn fn ) const = 0;
        virtual long long update( const string &stmt, resolve_fn fn ) const = 0;

        shared_ptr<Result> query( const string &stmt ) const {
            return query(stmt, [](const string& n) -> Value {
                return Value();
            });
        }
        size_t update( const string &stmt ) const {
            return update( stmt, [](const string &n) -> Value {
                return Value();
            });
        }
    };

    shared_ptr<Conn> connect( const string &dsn );

    // Makes it easy to handle savepoints in exception handlers
    class Savepoint {
        Conn &_conn;
        bool _released;
        string _name;
    public:
        Savepoint( Conn &conn, const string &name );
        ~Savepoint();

        void release();
    };

}