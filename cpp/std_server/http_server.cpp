/**
    Networking TS simple http server
*/

#include <experimental/executor>
#include <experimental/io_context>
#include <experimental/timer>
#include <experimental/buffer>
#include <experimental/socket>
#include <experimental/internet>
#include <regex>
#include <iostream>
#include <memory>
#include <map>
#include "tools.hpp"
#include "request_http.hpp"
#include "router.hpp"

using namespace tools;
using namespace std;
namespace net = std::experimental::net;
using net::ip::tcp;

class HttpConnectionHandler : public enable_shared_from_this<HttpConnectionHandler>
{
    tcp::socket _sock;
    string _sbuf;

public:
    HttpConnectionHandler( tcp::socket &&sock ) : _sock( move( sock )) {}

    void communicate() {
        using namespace acgi;

        shared_ptr<HttpConnectionHandler> ref( shared_from_this()); // Keep alive until ref is discarded

        async_read_http_request( move( _sock ), [this, ref]( shared_ptr<Request> req ) -> void {
            // cout << "header : " << req << endl;

            Router::inst().dispatch( req, []() {
                // Done handling
            });
        });
    }
};

template <class ProtocolHandler> class AcceptHandler {
    net::io_context &_ctx;
    tcp::resolver _resolver;

public:
    AcceptHandler( net::io_context &ctx ) : _ctx( ctx ), _resolver( ctx ) {}

    void connect( const string &host, const string &service ) {
        // cout << "Resolve " << host << endl;

        _resolver.async_resolve( host, service, [this, host](const std::error_code &ec, tcp::resolver::results_type results ) -> void {
            if( !ec ) {
                for( auto end_point: results ) {
                    cout << "host " << host << " resolves to " << end_point.service_name() << endl;

                    auto acc = make_shared<tcp::acceptor>( _ctx );

                    acc->open( tcp::v4() );
                    acc->set_option(tcp::acceptor::reuse_address(true));
                    acc->bind( end_point );
                    acc->listen();

                    accept( acc );
                }
            } else {
                cerr << "Error resolving " << host << ": " << ec << endl;
            }
        });
    }

protected:
    void accept( shared_ptr<tcp::acceptor> acc ) {
        acc->async_accept([this, acc]( const std::error_code &ec, tcp::socket peer ) -> void {
            if( !ec ) {
                // cout << "accept " << peer.remote_endpoint() << " from user " << endl;

                make_shared<ProtocolHandler>( move( peer ))->communicate();

                accept( acc ); // Play it again
            }
        });
    }
};

int main( int argc, char *argv[] )
{
    net::io_context ctx;

    AcceptHandler<HttpConnectionHandler> hndl( ctx );

    acgi::Router::inst().append( "^/$", []( shared_ptr<acgi::Request> req ) -> void {
        req->body_out( "Hello, root world" );
    }).append( "^/count$", []( shared_ptr<acgi::Session> sess, shared_ptr<acgi::Request> req ) -> void {
        if( !sess->has_a( "counter" ))
            sess->set( "counter", 0 );
        else {
            int cnt = any_cast<int>( sess->get( "counter" ));
            cnt++;
            sess->set( "counter", cnt );
        }

        req->body_out( "Counter " + to_string( any_cast<int>(sess->get( "counter" ))));
    }).default_handler_set([]( shared_ptr<acgi::Request> req ) -> void {
        req->body_out( "Hello, default world" );
    });

    hndl.connect( "localhost", "8080" );

    ctx.run();
}