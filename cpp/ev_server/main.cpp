#include "server.hpp"
#include "protocol_http.hpp"

#include <unistd.h>
#include <iostream>

using namespace std;

int main( int argc, char *argv[] )
{
    using namespace net;

    cout << "try to setup server in localhost:8080" << endl;

    Server svr( []( shared_ptr<Request> req ) -> HTTPStatus {
        req->body_set( "pong" );

        return HTTP_OK;
    }, 30, 100);

    svr.http_setup( "localhost", 8080 );

    ev::default_loop loop;

    loop.run();
}