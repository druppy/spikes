#include "handler.hpp"
#include "restful.hpp"
#include "../net/url.hpp"
#include "../value/json.hpp"
#include <regex>
#include <sstream>
#include <iostream>

using namespace fuwu;
using namespace core;

vector<string> split(const string &text, char sep) {
    vector<string> tokens;
    size_t start = 0, end = 0;
    while ((end = text.find(sep, start)) != string::npos) {
        string s = text.substr(start, end - start);
        start = end + 1;

        if( !s.empty())
            tokens.push_back( s );
    }
    tokens.push_back(text.substr(start));
    return tokens;
}

// Convert a string to a given type (limited range of types)
static Value value_conv( const type_info &t, const string &s )
{
    Value v;

    if( t == typeid( int ))
        v = Value( stoi( s ));
    else if( t == typeid( long ))
        v = Value( stol( s ));
    else if( t == typeid( long long ))
        v = Value( stoll( s ));
    else if( t == typeid( double ))
        v = Value( stold( s ));
    else if( t == typeid( string ))
        v = Value( s );
    else if( t == typeid( bool ))
        v = Value( s == "true");
    else if( t == typeid( cmap )) {
        stringstream ss( s );
        Value tmp( json_parse( ss ));

        if( tmp.type_info_get() == typeid( t ))
            v = tmp;
    } else if( t == typeid( cvector )) {
        stringstream ss( s );
        Value tmp( json_parse( ss ));

        if( tmp.type_info_get() == typeid( t ))
            v = tmp;
    }

    return v;
}

class RestfulHandler : public Handler {
    string _basepath;
    regex _uri_query;
    regex _uri_key;
    regex _uri_entity;
    regex _sort_reg;
    regex _range_reg;
public:
    RestfulHandler() : Handler( "restful" ) {
        _basepath = "/restful/";

        _uri_entity = _basepath + "([^?/]+)";
        _uri_query = _basepath + "([^?/]+)[^?]?\?([^?]+)";
        _uri_key = _basepath + "([^/]+)/(.+)";
        _sort_reg = "sort\\(([^)]+)\\)";
        _range_reg = "items=(\\d+)-(\\d+)";
    }

protected:
    void handle( uWsgi::Request &req, Session &sess ) const {
        Value res;

        auto i = req.path_info().find( _basepath );
        if( i == string::npos ) // Not a prober base uri
            return; // 500 bad uri

        smatch m;
        string uri = req.uri();
        if( !regex_search(uri, m, _uri_entity ))
            return; // 500 bad uri

        Value args = json_parse( req.read_body());

        string entity_name = m[ 0 ];
        if( !EntityDispatcher::inst().has_a( entity_name ))
            return; // 404 not found

        entity_ptr ent = EntityDispatcher::inst().find( entity_name );

        if( req.method() == "GET") { // Find a single or a list
            if( regex_match(req.uri(), _uri_query )) {
                net::URL url( req.uri());

                cmap cargs;
                vector<string> sort;

                // Parse known arguments and ignore everything else
                auto args = ent->arguments_get();
                for( auto arg : args ) {
                    for( auto o: url.options()) {
                        if( o.first == arg.name )
                            cargs[ arg.name ] = value_conv( arg.type, o.second );

                        if( o.first.find( "sort(" )) {
                            if( regex_search( o.first, m, _sort_reg ))
                                sort = split( m[0], ',' );
                        }
                    }

                    if(arg.optional == false && cargs.count( arg.name )) {
                        clog << "ERROR: argument '" << arg.name << "' not optional for entity '" << m[ 0 ] << "'" << endl;
                        return; // 500
                    }
                }

                // provide the http header range for the list function
                long long limit = -1, offset = -1;
                bool range = false;
                if( !req.get( "Range" ).empty()) {
                    string s = req.get( "Range" );

                    if( regex_search( s, m, _range_reg )) {
                        long long from = stoll( m[ 0 ] );
                        long long to = stoll( m[ 1 ] );

                        limit = (to - from) + 1;
                        offset = from;
                        range = true;
                    }
                }

                ListResult r = ent->do_list(sess, cargs, sort, limit, offset );

                res = Value( r.data );

                if( range && r.data.size() < r.total ) {
                    stringstream os;
                    os << "items " << r.offset << "-" << r.offset + r.data.size();

                    if( r.total > 0 && r.total > r.data.size() )
                        os << "/" << r.total;

                    req.add( "Accept-Ranges", "items" );
                    req.add( "Content-Range", os.str());
                }
            } else if( regex_search(uri, m, _uri_key )) {
                string key = m[ 1 ];

                // The key is always a string, we may need a convesion here too ...
                res = Value( ent->do_get( sess, Value( key )));
            }
        } else if( req.method() == "HEAD" ) { // does the resource exist
            res = Value( true ); // 200 ?
        } else if( req.method() == "POST" ) { // create a new
            res = Value( ent->do_create(sess, args));
        } else if( req.method() == "PUT" ) { // update an existing
            if( regex_search(uri, m, _uri_key )) {
                auto key = Value( m[ 1 ] );

                res = Value(ent->do_update( sess, key, args ));
            } else {
                // Error
            }
        } else if( req.method() == "DELETE" ) { // Delete an existing
            if( regex_search(uri, m, _uri_key )) {
                auto key = Value( m[ 1 ] );

                res = Value( ent->do_delete(sess, key));
            } else {
                // Error
            }
        } else {
            clog << "ERROR: unknown method '" << req.method() << "' for entity " << entity_name << endl;
            return;
        }

        stringstream os;
        json_serialize(os, Value( res ));
        req.set_body(os.str());
    }
};

static RestfulHandler restful_handler;

EntityDispatcher &EntityDispatcher::inst()
{
    static EntityDispatcher *inst;

    if( !inst )
        inst = new EntityDispatcher();

    return *inst;
}

void EntityDispatcher::reg( const string &name, entity_ptr ent )
{
    _entities[ name ] = ent;
}

void EntityDispatcher::unreg( const string &name )
{
    _entities.erase( name );
}

bool EntityDispatcher::has_a( const string &name ) const
{
    return _entities.count( name ) > 0;
}

entity_ptr EntityDispatcher::find( const string &name ) const
{
    auto i = _entities.find( name );

    if( i != _entities.end())
        return i->second;

    throw invalid_argument( name + " is not a known entity in dispacher");
}
