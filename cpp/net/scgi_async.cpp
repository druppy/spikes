/**
    Receiver class for SCGI in a async form.

    SCGI starts with a size of the header and then a null terminated
    header, and then a normal body.

    The point is to read the header in chunks and make sure to signal the
    readen size to the caller, to enable the caller to only read until the
    header end.

    netstring: 4:test,
*/
#include <map>
#include <iostream>
#include <cstring>
#include <regex>
#include <experimental/random>

using namespace std;

class SCGIParser {
    bool _gotit;
    int _size, _read_cnt;
    string _head_buf;
    map<string,string> _header_in,
        _header_out;
public:
    SCGIParser() : _gotit( false ), _size( -1 ), _read_cnt( 0 ) {}

    size_t inject( const char *buf, size_t size ) {
        if( _gotit || size < 1 )
            return 0;

        if( _size == -1 ) {
            size_t i = 0;
            for(; i != size; i++ ) {
                if( isdigit( buf[ i ] ))
                    _head_buf += buf[ i ];
                else if( buf[ i ] == ':' ) {
                    _size = atoi( _head_buf.c_str());
                    _head_buf.clear();
                    ++i;
                    break;
                } else
                    cerr << "bad netstring header" << endl;
            }

            return inject( &buf[ i ], size - i ) + i;
        } else {
            if( _head_buf.size() < _size ) {
                if(size + _head_buf.size() > _size )
                    size = _size - _head_buf.size();

                _head_buf.append( buf, size );

                if( _size == _head_buf.size() && _head_buf.back() == ',' ) {
                    _gotit = true;
                    parse_header();
                }
            }
        }

        return size;
    }

    bool has_header() const { return _gotit; }

    string get( const string &name ) const {return _header_in.find( name )->second;}
protected:
    void parse_header() {
        auto s = _head_buf.begin();
        string name;
        for( auto i = _head_buf.begin(); i != _head_buf.end(); i++ ) {
            if( *i == '\0' ) {
                if( name.empty()) {
                    name = string( s, i );
                } else {
                    _header_in[ name ] = string( s, i );
                    name.clear();
                }
                s = i; s++;
            }
        }
    }
};

const char test_header[] =
    "93:"
    "CONTENT_LENGTH\0" "4" "\0"
    "SCGI\0" "1" "\0"
    "REQUEST_URI\0/test.com\0"
    "METHOD\0GET\0"
    "TEST\0value\0"
    "CONTENT_TYPE\0plain/text\0,"
    "body";

int main() {
    using namespace experimental;

    size_t len = sizeof( test_header );

    cout << "size of " << len << endl;

    long cnt = 0;

    while( true ) {
        size_t i = 0;

        SCGIParser p;

        while( true ) {
            size_t n = randint( 1, (int)(len - i));

            size_t rn = p.inject( &test_header[ i ], n );

            i += rn;

            if( p.has_header() )
                break;

            if( len == i ) {
                cerr << "missing data" << endl;
                break;
            }
        }

        // cout << "first buffer char at pos " << i << " = " << test_header[ i ] << endl;
        cout << "count " << cnt++ << endl;

        int lenght = atoi( p.get( "CONTENT_LENGTH" ).c_str());

        string content( &test_header[ i ], lenght );
        if( content != "body" || p.get( "TEST" ) != "value" ) {
            cerr << "bad content (" << content.size() << "): '" << content << "' ( " << i << " )" << endl;
            break;
        }
    }
}