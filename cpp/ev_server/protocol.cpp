#include "protocol.hpp"

#include <unistd.h>
#include <sstream>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>

#include <streambuf>
#include <iostream>
#include <regex>

namespace net {
    string string_tolower( const string &str )
    {
        string res;
        res.reserve( str.length());
        for( auto i: str )
            res += (char)tolower( i );

        return res;
    }

    void socket_timeout_set( int fd, unsigned secs )
    {
        struct timeval tv {
            .tv_sec = secs,
            .tv_usec = 0
        };

        if( -1 == setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv))
            throw system_error( error_code( errno, std::generic_category() ) );
    }

    void socket_nonblock_set( int fd )
    {
        if( -1 == fcntl( fd, F_SETFL, fcntl( fd, F_GETFL, 0 ) | O_NONBLOCK ))
            throw system_error( error_code( errno, std::generic_category() ) );
    }

    /**
       Special class that first try to get all available data before blocking

       More and less directly from josuttis book, thanks.
     */
    class sock_inbuf : public std::streambuf {
        int _fd;
        char _buffer[ BUFSIZ ];
        ssize_t _maxLength, _count;
    public:
        sock_inbuf( int fd ) : _fd( fd ) {
            setg( _buffer + 4,
                  _buffer + 4,
                  _buffer + 4 );

            _maxLength = -1;  // Not in use
            _count = 0;
        }

        virtual ~sock_inbuf() {}

        void setMaxLength( ssize_t nLength ) {
            if( nLength != -1 )
                _maxLength = nLength + _count - (egptr() - gptr());
            else
                _maxLength = -1;
        }
    protected:

        virtual int_type underflow_simple() {
            if(gptr() < egptr())
                return *gptr();

            int num;
            if((num = recv(_fd, reinterpret_cast<char*>(_buffer), BUFSIZ, 0)) <= 0)
                return traits_type::eof();

            setg(_buffer, _buffer, _buffer + num);
            return *gptr();
        }

        virtual int_type underflow( void ) {
            // Is read buffer before the end ?
            if( gptr() > egptr())
                return traits_type::to_int_type( *gptr());

            // Putback ?
            int putback = gptr() - eback();
            if( putback > 4 )
                putback = 4;

            if( putback > 0 )
                memcpy( _buffer+(4-putback), gptr()-putback, putback ); // XXX Is this optimal ?

            if( _maxLength != -1 )
                if( _maxLength <= _count )
                    return EOF;

            // First try using non-block
            int cnt;
            do {
                int nLen = BUFSIZ - 4;

                if( _maxLength != -1 ) {
                    if( _count >= _maxLength )
                        return EOF;

                    nLen = (nLen < _maxLength - _count) ? nLen : _maxLength - _count;
                }

                if( nLen == 0 )
                    return EOF;

                if((cnt = read( _fd, _buffer + 4, nLen )) > 0 )
                    _count += cnt;
                else
                    throw system_error( error_code( errno, std::generic_category() ) );
            } while( cnt <= 0 );

            setg( _buffer + (4 - putback),
                  _buffer + 4,
                  _buffer + 4 + cnt);

            return traits_type::to_int_type( *gptr());
        }
    };

    class SockIstream : public std::istream {
        sock_inbuf _buf;
    public:
        SockIstream( int fd ) : _buf( fd ) {
            rdbuf( &_buf );
        }

        void setMaxLength( uint64_t nMaxBytes ) {
            _buf.setMaxLength( nMaxBytes );
        }
    };

    void Pipe::fire_on_ready()
    {
        for( auto el: _on_ready )
            el();
    }

    void Pipe::on_ready( callback ready_cb )
    {
        _on_ready.push_back( ready_cb );
    }

    static map<HTTPStatus, string> http_status = {
        {HTTP_CONTINUE,                 "Continue"},
        {HTTP_SWITCHING_PROTOCOLS,      "Switching Protocols"},
        {HTTP_OK,                       "OK"},
        {HTTP_CREATED,                  "Created"},
        {HTTP_ACCEPTED,                 "Accepted"},
        {HTTP_NO_CONTENT,               "No Content"},
        {HTTP_MOVED_PERMANENTLY,        "Moved Permanently"},
        {HTTP_MOVED_TEMPORARILY,        "Moved Temporarily"},
        {HTTP_NOT_MODIFIED,             "Not Modified"},
        {HTTP_BAD_REQUEST,              "Bad Request"},
        {HTTP_UNAUTHORIZED,             "Unauthorized"},
        {HTTP_FORBIDDEN,                "Forbidden"},
        {HTTP_NOT_FOUND,                "Not Found"},
        {HTTP_INTERNAL_SERVER_ERROR,    "Internal Server Error"},
        {HTTP_NOT_IMPLEMENTED,          "Not Implemented"},
        {HTTP_BAD_GATEWAY,              "Bad Gateway"},
        {HTTP_SERVICE_UNAVAILABLE,      "Service Unavailable"},
        {HTTP_RESET_CONTENT,            "Reset Content"},
        {HTTP_PARTIAL_CONTENT,          "Partial Content"},
        {HTTP_USE_PROXY,                "Use Proxy"},
        {HTTP_TEMPORARY_REDIRECT,       "Temporary Redirect"},
        {HTTP_METHOD_NOT_ALLOWED,       "Method Not Allowed"},
        {HTTP_NOT_ACCEPTABLE,           "Not Acceptable"},
        {HTTP_PROXY_AUTHENTICATION_REQUIRED,"Proxy Authentication Required"},
        {HTTP_REQUEST_TIMEOUT,          "Request Timeout"},
        {HTTP_CONFLICT,                 "Conflict"},
        {HTTP_GONE,                     "Gone"},
        {HTTP_LENGTH_REQUIRED,          "Length Required"},
        {HTTP_PRECONDITION_FAILED,      "Precondition Failed"},
        {HTTP_REQUEST_ENTITY_TOO_LARGE, "Request Entity Too Large"},
        {HTTP_REQUEST_URI_TOO_LONG,     "Request Uri Too Long"},
        {HTTP_UNSUPPORTED_MEDIA_TYPE,   "Unsupported Media Type"},
        {HTTP_REQUESTED_RANGE_NOT_SATISFIABLE,"Requested Range Not Satisfiable"},
        {HTTP_EXPECTATION_FAILED,       "Expectation Failed"},
        {HTTP_GATEWAY_TIMEOUT,          "Gateway Timeout"},
        {HTTP_VERSION_NOT_SUPPORTED,    "Version Not Supported"}
    };

    string Request::header_normalize( const string &name ) const
    {
        string lname = string_tolower( name );

        replace( lname.begin(), lname.end(), '-', '_' );

        return lname;
    }

    Request::Request( weak_ptr<Protocol> prot, int sock, const string &method, const headers_t &headers_in )
    {
        _sock = sock;
        _method = method;
        _header_send = false;
        for( auto h: headers_in )
            _headers_in[ header_normalize(h.first)] = h.second;

        protocol_set( prot );
    }

    Request::Request( weak_ptr<Protocol> prot, int sock )
    {
        _sock = sock;
        _header_send = false;

        protocol_set( prot );
    }

    Request::Request()
    {
        _sock = -1;
        _header_send = false;
    }

    void Request::protocol_set( weak_ptr<Protocol> prot )
    {
        _prot = prot;
    }

    bool Request::append( const string &name, const string &val )
    {
        _headers_out[ name ] = val;
        return true;
    }

    string Request::get( const string &name ) const
    {
        for( auto h: _headers_in )
            if( h.first == header_normalize( name ) )
                return h.second;

        throw invalid_argument( name + " is not a header in element" );
    }

    bool Request::has_a( const string &name ) const
    {
        for( auto h: _headers_in )
            if( h.first == header_normalize( name ) )
                return true;

        return false;
    }

    // Std CGI elements
    string Request::method() const
    {
        return _method;
    }

    string Request::content_type() const
    {
        return get( "content_type" );
    }

    string Request::uri() const
    {
        return _uri;
    }

    string Request::remote_addr() const {
        return "";
    }

    string Request::protocol() const
    {
        return "http";
    }

    string Request::script_name() const
    {
        return "";
    }

    string Request::host() const
    {
        return get( "host" );
    }

    string Request::user_agent() const
    {
        return get( "user_agent" );
    }

    string Request::document_root() const
    {
        return ""; // config option
    }

    string Request::encoding() const
    {
        return "";
    }

    string Request::referer() const
    {
        return get( "referer" );
    }

    string Request::cookie() const
    {
        return get( "cookie" );
    }

    string Request::path_info() const
    {
        return "";
    }

    string Request::authorization() const
    {
        return "";
    }

    bool Request::prepare_headers( ostream &os, HTTPStatus status )
    {
        stringstream h;

        h << status << " " << http_status[ status ];

        return prepare_headers( os, h.str());
    }

    bool Request::prepare_headers( ostream &os, const string &st )
    {
        if( os.good() ) {
            os << "HTTP/" << _version << " " << st << "\r\n";

            // send header fields
            for( auto i: _headers_out ) {
                if( os.good() )
                    os << i.first << ": " << i.second << "\r\n";
            }

            // Make ready for user content
            os << "\r\n";
            _header_send = true;
            return true;
        }

        return false;
    }

    bool Request::content_type_set( const string &mime_type )
    {
        return append( "Content-Type", mime_type );
    }

    bool Request::content_length_set( uint64_t length ) // write body does this by it self
    {
        return append( "Content-Length", to_string( length ));
    }

    void Request::body_set( const string &body, const string &mime_type )
    {
        content_type_set( mime_type );
        content_length_set( body.length() );

        _body = make_shared<StringPipe>( body );
    }

    void Request::body_set( shared_ptr<Pipe> body, const string &mime_type )
    {
        content_type_set( mime_type );
        if( body->total_size() > -1 )
            content_length_set( body->total_size() );

        _body = body;
    }

    static string make_etag( struct stat &stat )
    {
        stringstream etag;

        etag << hex << "\"" << stat.st_ino << "-" << stat.st_mtime << "-" << stat.st_size << "\"";

        // return md5( etag.str() );
        return etag.str();
    }

    // scanning the bytes pattern of the range carefully
    static regex bytes_pattern( "bytes=([0-9^\\-]+)\\-([0-9]*)" );
    static bool scan_range_bytes( const string &s, uint64_t &from, uint64_t &to )
    {
        smatch sm;

        if( regex_match(s, sm, bytes_pattern) ) {
            if( sm.size() == 3 ) {
                if( sm[ 1 ].length() > 0 )
                    from = stoll(sm[ 1 ]);

                if( sm[ 2 ].length() > 0 )
                    to = stoll( sm[ 2 ]);

                return true;
            }
        }
        return false;
    }

    net::HTTPStatus Request::body_file_set( const string &location, const string &mime_type, ssize_t offset, ssize_t length ) {
        struct stat stat_buf;

        if( 0 == stat( location.c_str(), &stat_buf )) {
            // Calculate etag, set it and test if it match user provided
            string etag = make_etag( stat_buf );

            append( "ETag", etag );

            if( has_a( "If-None-Match" )) {
                if( etag == get( "If-None-Match" ) || "*" == get( "If-None-Match" ) ) {
                    return net::HTTP_NOT_MODIFIED;
                }
            }

            append( "Content-Type", mime_type );

            if( _method == "HEAD" ) {
                append( "Content-Length", to_string( stat_buf.st_size ));
            } else {
                auto res = net::HTTP_OK;

                // Make sure to only auto detect range when not given by user
                if( offset == -1 && has_a( "Range" )) {
                    uint64_t part_from = 0, part_to = 0;

                    if( scan_range_bytes( get( "Range" ), part_from, part_to )) {
                        if( part_to > 0 && part_from > part_to ) {
                            clog << "bad HTTP range values " << get( "Range" ) << endl;
                            return net::HTTP_REQUESTED_RANGE_NOT_SATISFIABLE;
                        }

                        length = ( part_to > 0 ) ? part_to - part_from + 1 : -1;
                        offset = part_from;
                    }
                }

                int fd = open( location.c_str(), O_RDONLY );

                if( fd == -1 )
                    throw invalid_argument(strerror(errno));

                if( offset != -1 )
                    length = stat_buf.st_size;
                else {
                    if( length == 0 || length > stat_buf.st_size - offset )
                        length = stat_buf.st_size - offset;

                    if( length != stat_buf.st_size ) {
                        stringstream ros;

                        append( "Accept-Ranges", "bytes" );
                        ros << "bytes " << offset << "-" << (offset + length - 1) << "/" << stat_buf.st_size;
                        append( "Content-Range", ros.str());

                        if( offset != 0 )
                            lseek( fd, offset, SEEK_SET );

                        res = net::HTTP_PARTIAL_CONTENT;
                    }
                }

                append( "Content-Length", to_string( length ));

                auto p = make_shared<SocketPipe>( fd, length );

                p->owner_set();

                _body = p;

                return res;
            }
        }

        return net::HTTP_OK;
    }

    shared_ptr<istream> Request::body_read() const
    {
        if( has_a( "Content-Length" )) {
            // make sure the socket is sync when using it in a c++ stream
            if( -1 == fcntl(_sock, F_SETFL, fcntl(_sock, F_GETFL, 0) & ~O_NONBLOCK))
                throw invalid_argument( strerror( errno ));

            auto is = make_shared<SockIstream>( _sock );

            auto len = stoull( get( "Content-Length" ));

            is->setMaxLength(len);

            return is;
        }

        throw invalid_argument( "missing body length when making body read stream" );
    }

    string Request::cookie_get( const string &name ) const
    {
        return get( "cookie" );
    }

    string Request::str() const
    {
        stringstream is;

        is << "URI: " << _uri << endl;
        is << "Method: " << _method << endl;

        for( auto h: _headers_in ) {
            is << h.first << ": " << h.second << endl;
        }

        return is.str();
    }

    void Request::finish( HTTPStatus res )
    {
        if( !_prot.expired() )
            _prot.lock()->handle_finish( this, res );
    }

    /// StringPipe
    ssize_t StringPipe::read( char *buf, ssize_t len )
    {
        if( _done )
            return -1;

        if( _pos + 1 > _buf.length()) {
            _done = true;
            return -1;
        }

        if((_pos + len) > _buf.length()) {
            len = _buf.length() - _pos;
            _done = true;
        }

        len = _buf.copy( buf, len, _pos );
        _pos += len;
        return len;
    }

    void StringPipe::unread( char *buf, size_t len )
    {
        if( len < _pos )
            _pos -= len;
    }

    void StringPipe::set( const string &str )
    {
        _buf = str;
        _pos = 0;
        _done = false;
        _on_ready.clear();
    }

    ////////////////
    // SocketPipe

    SocketPipe::SocketPipe( int sock, size_t total_size ) :
        _sock( sock ),
        _buf_size( 0 ),
        _read_pos( 0 ),
        _total_size( total_size ),
        _owner( false ),
        _errno( 0 )
    { // force socket to async !
        socket_nonblock_set(sock);

        _read_io.set<SocketPipe, &SocketPipe::io_read>( this );
    }

    SocketPipe::~SocketPipe()
    {
        _read_io.stop();

        if( _owner && _sock > 0 )
            ::close( _sock );
    }

    void SocketPipe::stop()
    {
        _done = true;
        _read_io.stop();
        _on_ready.clear();
    }

    void SocketPipe::io_read( ev::io &r, int revents )
    {
        if( _done )
            _read_io.stop();

        if(ev::ERROR & revents)
            return;

        if(revents & ev::READ)
            fire_on_ready();
    }

    ssize_t SocketPipe::read( char *buf, ssize_t len )
    {
        // read the unread buffer first
        if( _buf_size > 0 ) {
            if( len >= _buf_size ) {
                len = _buf_size;
                _buf_size = 0;
                memcpy( buf, _buf, len );
                return len;
            }

            assert( len > _buf_size ); // we may need to support this, but it would cost memcpy for rotating
        }

        if( _done )
            return -1;

        // if total size is set, make sure to handle it
        if( _total_size != -1 )
            if( _total_size < len + _read_pos )
                len = _total_size - _read_pos;

        ssize_t s = ::read( _sock, buf, len );
        if( s == 0 ) {
            stop();
            return -1;
        } else if( s != 0 ) {
            if( errno == EAGAIN || errno == EWOULDBLOCK ) {
                if( !_read_io.is_active())
                    _read_io.start( _sock, ev::READ );

                return 0;
            }
            _errno = errno;
            clog << "file socket read error " << strerror( errno ) << endl;
            stop();
        } else if( s > 0 ) {
            _read_pos += s;

            if( _read_pos == _total_size )
                stop();

            _read_io.stop();
        }

        return s;
    }

    void SocketPipe::unread( char *buf, size_t len )
    {
        if( _buf_size != 0 )
            throw invalid_argument( "unread can only be set once" );

        if( len > sizeof( _buf ))
            throw invalid_argument( "unread buffer too large" );

        memcpy( _buf, buf, len );
        _buf_size = len;
    }
}
