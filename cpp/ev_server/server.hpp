/**
   All the basic code for async accept of incomming connections using libev as acync
   manager of connections.

   This bridge code will accept on multible sockets, and accept incomming connections
   as fast as possible and put the accepted socket on a command queue, and go back to listen
   again.

   After the command queue, the socket will be changed to sync mode and we will use the
   nbpp SCGI og HTTP parser to handle the protocol, but in its own thread.

   When the Request parsing have been performed, all requests are dispatched to the router
   instance, and the prober route are found and processing the request will be done using
   the matched route.
*/
#include <ev++.h>
#include <functional>
#include <vector>
#include <list>
#include <string>
#include <netinet/in.h>
#include <mutex>
#include <regex>
#include <condition_variable>

#include "protocol.hpp"

#pragma once

namespace net {
    using namespace std;

    using mimetypes_t = vector<string> ;

    typedef function<void(int, sockaddr_in *)> handler_t;

    // Special version that also handles lambda's
    class ev_io : public ev::io {
        using fn_t = function<void(ev::io &watcher, int revents)>;
        fn_t _fn;

        void call( ev::io &watcher, int revents ) {
            _fn( watcher, revents );
        }
    public:
        void set( int sfd, fn_t fn );
    };

    class ev_idle : public ev::idle {
        using fn_t = function<void(ev::idle &watcher, int revents)>;
        fn_t _fn;

        void call( ev::idle &watcher, int revents ) {
            _fn( watcher, revents );
        }
    public:
        void set( fn_t fn );
    };

    class ev_timer : public ev::timer {
        using fn_t = function<void(ev::timer &watcher, int revents)>;
        fn_t _fn;

        void call( ev::timer &watcher, int revents ) {
            _fn( watcher, revents );
        }
    public:
        void set( fn_t fn );
    };

    class ev_sig : public ev::sig {
        using fn_t = function<void( ev::sig &watcher, int revents )>;
        fn_t _fn;

        void call( ev::sig &watcher, int revents )
        {
            _fn( watcher, revents );
        }

      public:
        void set( fn_t fn );
    };

    class Server {
        list<ev_io> _ios;
        using handler_fn_t = function<HTTPStatus(shared_ptr<net::Request> req)>;
        handler_fn_t _handler;
        int _backlog;
        int _timeout_client; // seconds timeout on client receive
    public:
        Server( handler_fn_t fn, int timeout_client, int backlog ) : _handler( fn ), _backlog( backlog ), _timeout_client( timeout_client ) {}
        ~Server();

        void http_setup( string dns, int port = 8080 );
    protected:

        /**
            Start listening to a given DNS and port and call handler
            when connection occurs.
        */
        void bind_inet( handler_t cb, const std::string &dns, int port );
        void bind_sock( handler_t cb, const std::string &sock_path );

        void listen( int sfd, handler_t cb );
    };
}
