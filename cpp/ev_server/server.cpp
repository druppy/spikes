/**
    The code should also allow for BSD (OSX) builds too.

    This is also one step in the direction of getting rid of nb++ :-)
*/
#include "server.hpp"
#include "protocol_http.hpp"

#include <map>
#include <memory>
#include <sstream>
#include <functional>
#include <vector>
#include <list>
#include <utility>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <ev++.h>
#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include <sys/un.h>

using namespace std;
using namespace net;

namespace net {
    static void set_socket_linger(int fd)
    {
        struct linger so_linger {
            .l_onoff  = 1,
            .l_linger = 1
        };

        if( -1 == setsockopt(fd, SOL_SOCKET, SO_LINGER, &so_linger, sizeof so_linger))
            throw system_error( error_code( errno, std::generic_category() ) );
    }

    void ev_io::set( int sfd, fn_t fn )
    {
        _fn = fn;
        io::set<ev_io, &ev_io::call>( this );
    }

    void ev_idle::set( fn_t fn )
    {
        _fn = fn;
        idle::set<ev_idle, &ev_idle::call>( this );
    }

    void ev_timer::set( fn_t fn )
    {
        _fn = fn;
        timer::set<ev_timer, &ev_timer::call>( this );
    }

    void ev_sig::set( fn_t fn )
    {
        _fn = fn;
        sig::set<ev_sig, &ev_sig::call>( this );
    }

    Server::~Server()
    {
        // close all listeners
        for( auto &io: _ios) {
            io.stop();
            close( io.fd );
        }
    }

    void Server::http_setup( string dns, int port )
    {
        bind_inet([this](int sfd, sockaddr_in *addr) {
            // Set the value on the socket too
            socket_timeout_set(sfd, _timeout_client);

            auto prod = protocol_http( _timeout_client );

            prod->handler_set([this, prod]( shared_ptr<net::Request> req ) {
                req->finish( _handler( req ));
            });

            prod->handle_start( sfd );
        }, dns, port );
    }

    void Server::bind_inet( handler_t cb, const string &hostname, int port )
    {
        struct addrinfo hints, *result;

        memset(&hints, 0, sizeof(struct addrinfo));
        hints.ai_family = AF_UNSPEC;                /* Allow IPv4 or IPv6 */
        hints.ai_socktype = SOCK_STREAM;            /* Stream socket */
        if( !hostname.empty() )
            hints.ai_flags = /*AI_PASSIVE|*/AI_CANONNAME;   /* For wildcard IP address */

        stringstream sport;
        sport << port;

        int code;
        if((code = getaddrinfo(hostname.empty() ? nullptr : hostname.c_str(), sport.str().c_str(), &hints, &result )) == 0 ) {
            for (auto rp = result; rp != nullptr; rp = rp->ai_next) {
                // XXX fetch address info (see man)
                int sfd = socket(rp->ai_family, rp->ai_socktype,
                    rp->ai_protocol);

                if (sfd == -1)
                    throw system_error( error_code(errno, std::generic_category()));

                int one = 1;
                if (setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one)) < 0)
                    throw system_error( error_code(errno, std::generic_category()));

                struct sockaddr_in *addr = (struct sockaddr_in *)rp->ai_addr;

                // Make sure to set prober port too
                addr->sin_port = htons( port );

                if (bind(sfd, rp->ai_addr, rp->ai_addrlen) != 0) {
                    error_code err(errno, std::generic_category());
                    close( sfd );
                    freeaddrinfo( result );
                    throw system_error( err );
                }

                listen( sfd, cb );
                // clog << "listen to '" << inet_ntoa( addr->sin_addr ) << ":" << port << "'" << endl;
            }

            freeaddrinfo( result );
        } else
            throw invalid_argument( "getaddrinfo " + string(gai_strerror( code )));
    }

    void Server::bind_sock( handler_t cb, const string &sock_path )
    {
        int fd = socket( AF_UNIX, SOCK_STREAM, 0 );

        struct sockaddr_un addr;
        memset(&addr, 0, sizeof(addr));
        addr.sun_family = AF_UNIX;
        strncpy(addr.sun_path, sock_path.c_str(), sizeof(addr.sun_path)-1);

        if( bind(fd, (struct sockaddr*)&addr, sizeof(addr)) != 0)
            throw system_error( error_code(errno, std::generic_category()));

        listen( fd, cb );
    }

    void Server::listen( int sfd, handler_t fn )
    {
        socket_nonblock_set(sfd);

        int err = ::listen( sfd, _backlog );
        if( err != 0 ) {
            error_code err(errno, std::generic_category());
            close( sfd );
            throw system_error( err );
        }

        _ios.resize( _ios.size() + 1 );

        auto &io = _ios.back();
        io.priority = 1;
        io.set( sfd, [fn, sfd, this]( ev::io &watcher, int revents ) -> void {
            if (ev::ERROR & revents) {
                clog << "got invalid event om listen socket" << endl;
                return;
            }

            try {
                struct sockaddr saddr;

                for( int cnt = 0; cnt < _backlog / 2; ++cnt ) {
                    unsigned len = sizeof( saddr );

#ifdef _GNU_SOURCE
                    int sock = ::accept4( sfd, &saddr, &len, SOCK_NONBLOCK );
#else
                    int sock = ::accept( sfd, &saddr, &len );
#endif

                    if( sock < 0 ) {
                        if( errno == ECONNABORTED ) // Just get rid of this backlog entry
                            continue;

                        if( errno == EWOULDBLOCK || errno == EAGAIN || errno == EINTR ) // lets wait for next read event
                            break;

                        throw system_error( error_code(errno, std::generic_category()));
                    }

#ifndef _GNU_SOURCE
                    socket_nonblock_set( sock );
#endif

                    if (saddr.sa_family == AF_INET || saddr.sa_family == AF_INET6) {
                        sockaddr_in *sin = reinterpret_cast<sockaddr_in*>(&saddr);
                        fn(sock, sin);
                    } else {
                        clog << "unknown connection type in accept, closing handle" << endl;
                        ::close( sock );
                    }
                }
            } catch( const exception &ex ) {
                clog << "socket error " << ex.what() << endl;
                throw;
            }
        });
        io.start(sfd, ev::READ);
    }
}
