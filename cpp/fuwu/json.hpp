#ifndef __JSON_HPP__
#define __JSON_HPP__ 1

#pragma once

#include <mpark/variant.hpp>  // will be in C++17
#include <utility>
#include <vector>
#include <map>
#include <istream>
#include <ostream>

namespace core {
    using namespace std;
    using namespace mpark;

    struct node : variant<monostate, nullptr_t, bool,
            double, string, vector<node>,
            map<string, node>> {

        using string_type = string;
        using array_type = vector<node>;
        using object_type = map<string_type, node>;

        typedef variant<monostate, nullptr_t,
            bool, double, string, array_type, object_type> base_type;

        using base_type::base_type;

        node() : node(in_place_type<monostate>) {}

        node(char const* value) : node(in_place_type<string_type>, value) {}

        base_type& base() { return *this; }
        base_type const& base() const { return *this; }
    };

    using array = typename node::array_type;

    using object = typename node::object_type;

    using pair = typename object::value_type;

    void swap(node& lhs, node& rhs) noexcept {
        lhs.swap(rhs);
    }

	node json_parse( istream &is );
	ostream &json_serialize( ostream &os, const node &val );
}

#endif
