// define async protocol handler common structure (taken from https://github.com/druppy/spikes)
#pragma once

#include <ev++.h>
#include <functional>
#include <algorithm>
#include <map>
#include <memory>
#include <string>
#include <ostream>
#include <list>
#include <cassert>

namespace net {
    using namespace std;

    void socket_timeout_set( int fd, unsigned secs );
    void socket_nonblock_set( int fd );
    string string_tolower( const string &str );

    /**
        Pipe is a data flow that handle data in an async way

        Data can be read from the top of the pipe and every time new data arrive
        the on_ready callback are called.

        When we read data we may need to unread some or all of it to store it for later
        using the unread function.

        When a Pipe reads to the end and is done, all callbacks are removed too.

        This is not thread safe
    */
    class Pipe {
        using callback = function<void()>;
    protected:
        list<callback> _on_ready;
        bool _done;
        // Notify all about new data that is ready
        void fire_on_ready();

    public:
        Pipe() : _done( false ) {}
        virtual ~Pipe() {}

        /**
            When the pipe is empty and no more data is expected this will be true
        */
        bool done() const {return _done;}

        /**
            register function to be called when new data arrive
        */
        void on_ready( callback ready_cb );

        /**
            Return the number of callbacks that has been registered

            @return number of on_ready callbacks registered
        */
        list<callback>::size_type on_ready_size() const {
            return _on_ready.size();
        }

        /**
            Read some data from the pipe, if any. The call will read as many bytes into
            the buffer as possible and return the number of bytes copied.

            @param buf holds user provided buffer that is min len long
            @param len the length of the buffer

            @return
               n > 0   : n data loaded to buffer
               n == 0  : no data right now (will fire on_ready later)
               n == -1 : no more data found, or error
        */
        virtual ssize_t read( char *buf, ssize_t len ) = 0;

        /**
            make sure to mark len bytes as not read, so that we get these again on next read

            @param len is the number of bytes to unread
        */
        virtual void unread( char *buf, size_t len ) = 0; // move reading position len back

        /**
            Return the total size of the pipe content, if given.
        */
        virtual ssize_t total_size() const { return -1; }
    };

    enum HTTPStatus {
        HTTP_CONTINUE = 100,
        HTTP_SWITCHING_PROTOCOLS = 101,
        HTTP_OK = 200,
        HTTP_CREATED = 201,
        HTTP_ACCEPTED = 202,
        HTTP_NO_CONTENT = 204,
        HTTP_RESET_CONTENT = 205,
        HTTP_PARTIAL_CONTENT = 206,
        HTTP_MOVED_PERMANENTLY = 301,
        HTTP_MOVED_TEMPORARILY = 302,
        HTTP_NOT_MODIFIED = 304,
        HTTP_USE_PROXY = 305,
        HTTP_TEMPORARY_REDIRECT = 307,
        HTTP_BAD_REQUEST = 400,
        HTTP_UNAUTHORIZED = 401,
        HTTP_FORBIDDEN = 403,
        HTTP_NOT_FOUND = 404,
        HTTP_METHOD_NOT_ALLOWED = 405,
        HTTP_NOT_ACCEPTABLE = 406,
        HTTP_PROXY_AUTHENTICATION_REQUIRED = 407,
        HTTP_REQUEST_TIMEOUT = 408,
        HTTP_CONFLICT = 409,
        HTTP_GONE = 410,
        HTTP_LENGTH_REQUIRED = 411,
        HTTP_PRECONDITION_FAILED = 412,
        HTTP_REQUEST_ENTITY_TOO_LARGE = 413,
        HTTP_REQUEST_URI_TOO_LONG = 414,
        HTTP_UNSUPPORTED_MEDIA_TYPE = 415,
        HTTP_REQUESTED_RANGE_NOT_SATISFIABLE = 416,
        HTTP_EXPECTATION_FAILED = 417,
        HTTP_INTERNAL_SERVER_ERROR = 500,
        HTTP_NOT_IMPLEMENTED = 501,
        HTTP_BAD_GATEWAY = 502,
        HTTP_SERVICE_UNAVAILABLE =503,
        HTTP_GATEWAY_TIMEOUT = 504,
        HTTP_VERSION_NOT_SUPPORTED = 505
    };

    typedef map<string, string> headers_t;

    class Protocol;

    // Request structure as in uWsgi plugin
    class Request {
        Request( const Request & ) = delete;
        Request &operator=( const Request &) = delete;
    protected:
        weak_ptr<Protocol> _prot;
        int _sock;
        string _version;
        string _uri;
        shared_ptr<Pipe> _body;
        bool _header_send;
        headers_t _headers_in,
            _headers_out;
        string _method;
        // internal header normalizer use to access
        string header_normalize( const string &name ) const;
    public:
        typedef list<pair<float, string>> langs_t;

        Request();
        Request( weak_ptr<Protocol> prot, int sock );
        Request( weak_ptr<Protocol> prot, int sock, const string &method, const headers_t &headers_in );
        virtual ~Request() {};

        void protocol_set( weak_ptr<Protocol> prot );

        void socket_set( int sock ) {_sock = sock;}
        int socket() const { return _sock;}
        virtual void takeover() {_sock = -1;}

        bool append( const string &name, const string &val );

        string get( const string &name ) const;
        bool has_a( const string &name ) const ;

        string version() const {return _version;}

        // Std CGI elements
        string method() const;
        string content_type() const;
        string uri() const;
        string remote_addr() const;
        string protocol() const;
        string script_name() const;
        string host() const;
        string user_agent() const;
        string document_root() const;
        string encoding() const;
        string referer() const;
        string cookie() const;
        string path_info() const;
        string authorization() const;

        bool prepare_headers( ostream &os, HTTPStatus status = HTTP_OK );
        bool prepare_headers( ostream &os, const string &st = "200 OK" );
        bool content_type_set( const string &mime_type );
        bool content_length_set( uint64_t length ); // write body does this by it self
        // Send back this string body
        void body_set( const string &body, const string &mime_type = "plain/text" );

        // Send back content of this pipe
        void body_set( shared_ptr<Pipe> body, const string &mime_type = "plain/text" );

        /**
            Set the return type to this given file, and send back the file when done.

            This function sets a pipe, using the fd of the file, and it uses the file size
            from the stats given by the file system.

            Head requests are just returning etag, content length, and ok if file exists. All file
            delivered sets an etag. When no offset are given as argument, the Range http header are
            parsed and used for partial file delivery.

            When we only need to send parts of a file, the part_from and length can be used, and the
            prober header fields are set send and we seek the file to the relevant position, too.

            The return value is the return value need as return values for the request

            @param location holds the full path the the file to send
            @param mime_type holds the mimetype of the file
            @param offset holds the offset into the file where default is 0
            @param length holds the length of the data needed to be send

            @returns the status needed to terminate the request
        */
        net::HTTPStatus body_file_set( const string &location, const string &mime_type, ssize_t offset = 0, ssize_t length = -1 );

        // get the pipe holding data to the client
        shared_ptr<Pipe> body_pipe() const {return _body;}

        langs_t accept_language_list() const;
        float accept_language( const string &locale ) const;
        float accept_mimetype( const string &mime_type ) const;

        // Get a pipe to the body of this message
        shared_ptr<istream> body_read() const;

        string cookie_get( const string &name ) const;

        string str() const;

        void finish( HTTPStatus res );
    };

    // handle_sock -> protocol -> router
    typedef function<void(shared_ptr<Request> req)> request_handler_t;

    // Protocol connection handler base class
    class Protocol {
        Protocol( const Protocol &) = delete;
        Protocol &operator=( const Protocol &) = delete;
    protected:
        request_handler_t _fn;

    public:
        Protocol() {}
        Protocol( request_handler_t fn ) : _fn( fn ) {}

        virtual ~Protocol() {}

        void handler_set( request_handler_t fn ) { _fn = fn; }

        virtual void handle_start( int sock ) = 0;
        virtual void handle_finish( Request *req, HTTPStatus status ) = 0;

        virtual void takeover() {};
    };

    /**
        A pipe type build on a string.
    */
    class StringPipe : public Pipe {
        string _buf;
        size_t _pos;
    public:
        StringPipe( const string &str = "" ) : _buf( str ), _pos( 0 ) {}

        ssize_t read( char *buf, ssize_t len ) override;
        void unread( char *buf, size_t len ) override;

        ssize_t total_size() const override {return _buf.length();}

        // special string pipe set function
        void set( const string &str = "" );
    };

    /**
        Define a pipe based on a socket or a file descriptor. It will force the
        handler async, and will close the handler if it is made owner.

        If the size is given, it will stop reading when the max size are meet.
    */
    class SocketPipe : public Pipe {
        int _sock;
        char _buf[ BUFSIZ ];
        ssize_t _buf_size,
            _read_pos,
            _total_size;
        bool _owner;
        int _errno;
        ev::io _read_io;

        void stop();
        void io_read( ev::io &r, int revents ); // libev callback
    public:
        SocketPipe( int sock, size_t total_size = -1 );
        ~SocketPipe();

        /// owned is closing the handle on destruction
        void owner_set( bool owner = true ) { _owner = owner; }

        /// provide the max size of the pipe or -1
        ssize_t total_size() const override {return _total_size;}

        ssize_t read( char *buf, ssize_t len ) override;
        void unread( char *buf, size_t len ) override;
    };
}

inline std::ostream &operator<<( std::ostream &os, const net::Request &req ) { os << req.str() ; return os; }
