#include "request_http.hpp"
#include "tools.hpp"
#include <experimental/buffer>
#include <regex>
#include <iostream>

namespace acgi {
    using namespace tools;
    using namespace std;
    namespace net = std::experimental::net;
    using net::ip::tcp;

    const size_t max_body_size = 1024 * 1024;

    // method uri http/version
    static regex http_header("([[:alpha:]]+) (.+) HTTP/([[:digit:].]+)");

    size_t RequestHttp::length() const {
        auto len = get( "Content-Length" );

        if( !len.empty())
            return stoll( len );

        return 0;
    }

    string RequestHttp::split_request( const string &line, string &method, string &uri )
    {
        smatch match;
        string ver = "0.9";

        if( regex_search( line, match, http_header )) {
            if( match.size() == 4 ) {
                method = match.str( 1 );
                uri = match.str( 2 );
                ver = match.str( 3 );
            }
        }

        return ver;
    }

    void RequestHttp::header_send( tcp::socket &sock, bool close, function<void( const error_code &ec )> fn )
    {
        set( "Connection", close ? "close" : "keep-alive" );

        auto buf = make_shared<string>( "HTTP/1.1 200 OK\r\n" );

        for( auto h: _header_out )
            *buf += h.first + ": " + h.second + "\r\n";

        *buf += "\r\n";

        sock.async_send( net::buffer( *buf ), [this, buf, &sock, fn]( const error_code &ec, int len ) -> void {
            if( !ec ) {
                if( _body.empty() )
                    fn( ec );
                else {
                    sock.async_send( net::buffer( _body ), [fn]( const error_code &ec, int len ) -> void {
                        fn( ec );
                    });
                }
            } else
                fn( ec );
        });
    }

    size_t RequestHttp::parse_header( string_view header )
    {
        size_t pos = header.find_first_of("\r\n") + 2;
        _http_ver = split_request( string(header.substr( 0, pos )), _method, _uri );

        while( true ) {
            size_t i = header.find_first_of( "\r\n", pos );

            if( i != string::npos ) {
                i += 2;
                string_view l = header.substr( pos, i - pos );

                size_t p = l.find_first_of( ":" );
                if( p != string::npos ) {
                    _header_in.insert( make_pair(
                        string(trim( l.substr( 0, p ))),
                        string(trim( l.substr( p + 1 )))
                    ));
                    pos = i;
                    continue;
                }
            }

            break;
        }

        return pos;
    }

    const auto chunck_size = 1024;

    void RequestHttp::async_body_read( body_read_fn fn ) {
        if( _body_buf.lenght() > 0 ) {
            error_code ec;

            fn( ec, net::buffer( _body_buf ));

            _body_buf.empty();
        } else {
            size_t size = chunck_size;

            if( size < length() - _transfered)
                size = (lenght() - _transfered) % chunck_size;

            net::async_read( *sock, net::dynamic_string_buffer( req->_body_buf, size ),
                [fn, this, sock]( const error_code &ec, size_t bytes_transfered ) -> void {
                    _transfered += bytes_transfered;

                    fn( ec, net::buffer( _body ))
                }
            )
        }
    }

    void RequestHttp::result_set( size_t length, string_view mimetype )
    {
        set( "Content-Length", to_string( length ));
        set( "Content-Type", mimetype );
    }

    // Parse the request, build a Request and send back answer when done all in async
    void async_read_http_request( tcp::socket &&socket, request_fn fn )
    {
        auto buf = make_shared<string>();
        auto sock = make_shared<tcp::socket>( move( socket ));
        auto req = make_shared<RequestHttp>( sock );

        net::async_read_until(*sock, net::dynamic_buffer(*buf), "\r\n\r\n",
            [fn, buf, req, sock]( const error_code &ec, auto len ) -> void {
                if( !ec ) {
                    size_t pos = req->parse_header( string_view( &(*buf)[0], len ));

                    // if we have some of the body in our buffer save it for the request consumer
                    if( pos < len )
                        req->_body_buf = buf->substr( pos, pos - len );

                    if( req->length() > 0 ) {
                        // first get body part and then call request handler
                        // Test max length

                        fn( req );

                        // has result_set been called ?

                        req->header_send( *sock, true, [req, sock]( const error_code &ec ) -> void {
                            // Start sending the body if anyone needs to

                            // Really just capture request until done
                            if( !ec ) {
                                // if persistent 1.1 connection, loop
                                // async_read_http_request( move( *sock ), fn );
                            } else
                                cerr << "ERROR: send http responce " << ec << endl;
                        });

                    } else {
                        // No body found, just handle request without
                        fn( req );

                        req->header_send( *sock, true, [req, sock]( const error_code &ec ) -> void {
                            // Really just capture request until done
                            if( !ec ) {
                                // if persistent 1.1 connection, loop
                                // async_read_http_request( move( *sock ), fn );
                            } else
                                cerr << "ERROR: send http responce " << ec << endl;
                        });
                    }
                } else
                    cout << "ERROR: reading http header " << ec.message() << endl;
            }
        );
    }
}