# Event driven server

Goals for this server is :

 * listen to more than one socket
 * handle multiple protocols
 * handle DNS lookup in a bsd/osx/linux friendly way
 * use libev as base for accept
 * handle header parsing asynchronously 
 * handle simple routing

This is a proof on concept test service, doing all the protocol and connection work for normal backend systems. 

It tries to use async protocol handling as long as possible, until it the request are ready for processing, where a handling thread may take over if it is not handling all things in a async way ?   

2 protocols are possible :

 * http 1 - super simple for testing
 * scgi - for flexibility
 
The request handling of all these need to be the same, and is transparent. This makes it possible to setup services that is protocol agnostic and makes testing much more simple along with production code. 

We also like to explore the possibility of using ip v4 and v6 and unix sockets, and handle nice shutdown using signals too.

In order to send back data, there is a small pipe class that can hold any kind of data that we can fetch in an async manner. This class makes it possible to have a small and common interface to may kinds of data, and makes it possible to let the protocol handler send it back at its own pace.

## The connection flow

The server is able to handle large count of http requests, by making a protocol handler on every request. The flow is this :

### stating up

1. make socket 
2. set it in listen mode
3. let libev trigger an EV_READ everytime a new socket is accepted

### connection handling

1. on accept, make a new protocol handler class (shared_ptr)
2. protocol handler register callbacks into libev
3. call the `handle_start` on the protocol handle using the new socket
4. try to read the first in the socket
5. if no data on socket, wait for libev to post event
6. when header is parsed, make a call to user function using request 
7. when user fn is done call `handle_finish` with the result code.
8. send back header (from pipe), when socket is ready
9. if socket is not ready, while for EV_WRITE before sending back more
9. send back body (from pipe), when socket is ready


## Build test server

To test out the code, a test server can bed build. The test server is just a simple http server that listen on port 8080 and handles simple http 1.0 and it will return a valid pong answer for each request.

to build server, do this in a linux box :

```
cmake .
make
./test_svr 
```

To test out the server performance use tools like apache benchmark, where we can put on pressure on the test server using commands like :

`ab -c 500 -n 100000 http://localhost:8080/x`

This will try to send 100000 connections to the service with max 500 in parallel.