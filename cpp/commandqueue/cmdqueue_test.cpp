#include "cmdqueue.hpp"
#include <iostream>

int main( int argc, const char *argv[] )
{
    atomic_uint async_cnt{0};

    srand (time(NULL));

    CommandQueue<int> queue( [&async_cnt]( int cnt ) -> void {
        async_cnt += cnt;

        this_thread::sleep_for(chrono::milliseconds( cnt )); // simulate workload
    }, 25);

    unsigned sync_cnt = 0;
    for( unsigned i = 0; i != 1000; i++ ) {
        unsigned n = rand() % 1000;

        queue( n );

        sync_cnt += n;
    }

    cout << "pool size " << queue.pool_size() << ", working processes " << queue.working_count() <<
        ", queue size " << queue.queue_current_size() << endl;

    queue.wait();

    cout << "pool size " << queue.pool_size() << ", working processes " << queue.working_count() <<
        ", queue size " << queue.queue_current_size() << endl;


    if( sync_cnt == async_cnt )
        return 0;

    cerr << "summarized number is wrong" << endl;
    return 1;
}