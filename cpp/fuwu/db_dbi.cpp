#include "db.hpp"
#include <dbi/dbi.h>
#include <cstring>

using namespace std;
using namespace core;
using namespace DB;

class DBIConn;

Savepoint::Savepoint( Conn &conn, const string &name ) : _conn( conn ), _released( false ), _name( name )
{
    conn.savepoint(name);
}

Savepoint::~Savepoint()
{
    if( !_released )
        _conn.savepoint_roleback(_name);
}

void Savepoint::release()
{
    _conn.savepoint_release( _name );
    _released = true;
}

// parse statement string and interpolate data from fn, ex "WHERE id = $id"
static void parse_stmt( ostream &os, const Conn &conn, const string &stmt, resolve_fn fn )
{
    auto i = stmt.begin();

    while( i != stmt.end() ) {
        if( *i == '$' ) {
            ++i;

            if( *i == '$' ) {
                os << "$";
                ++i;
            } else if( isalpha( *i )) {
                string key;

                while( i != stmt.end() && (isalnum( *i ) || strchr( "_", *i )))
                    key += *i++;

                os << conn.escape( fn( key ) );
            } else {
                stringstream eos;

                eos << "SQL format syntax error at : " << (i - stmt.begin()) << " (" << *i << ")";
                throw invalid_argument( eos.str() );
            }
        } else
            os << *i;
    }
}

class DBIResult : public DB::Result {
    dbi_result _res;
public:
    DBIResult( dbi_result res ) : _res( res ) {}

    ~DBIResult() {
        dbi_result_free( _res );
    }

    bool next() {
        return dbi_result_next_row( _res );
    }

    size_t columns() const {
        return dbi_result_get_numfields(_res);
    }

    long long rows() const {
        return dbi_result_get_numrows(_res);
    }

    bool is_null( const string &field_name ) const {
        return 1 == dbi_result_field_is_null(_res, field_name.c_str());
    }

    Value get( const string &field_name ) const {
        auto t = dbi_result_get_field_type(_res, field_name.c_str());

        Value r;

        switch( t ) {
            case DBI_TYPE_DATETIME:
                r = Value( dbi_result_get_datetime( _res, field_name.c_str()) );
                break;

            case DBI_TYPE_BINARY:
                throw invalid_argument( "can't handle binary data, yet");

            case DBI_TYPE_DECIMAL:
                r = Value( dbi_result_get_double(_res, field_name.c_str()));
                break;

            case DBI_TYPE_INTEGER:
                r = Value( dbi_result_get_longlong( _res, field_name.c_str()));
                break;

            case DBI_TYPE_STRING:
            default:
                r = Value( dbi_result_get_string(_res, field_name.c_str()));
                break;
        };

        return r;
    }
};

// This need to be one per thread !
class DBIInstance {
    dbi_inst _inst;

public:
    DBIInstance() {
        dbi_initialize_r(NULL, &_inst);
    }

    ~DBIInstance() {
        dbi_shutdown_r(_inst);
    }

    dbi_conn conn_new( const string &type_name ) {
        dbi_conn conn = dbi_conn_new_r( type_name.c_str(), _inst );

        return conn;
    }
};

static DBIInstance inst;

static vector<string> split( const string &str, const string &delim )
{
    vector<string> res;

    auto start = 0U;
    auto end = str.find(delim);
    while (end != string::npos)
    {
        res.push_back( str.substr(start, end - start));
        start = end + delim.length();
        end = str.find(delim, start);
    }

    res.push_back( str.substr(start, end));
    return res;
}

// parse a dsn and set options on connection
static dbi_conn parse_dsn( const string &dsn )
{
    string dbtype = "pgsql";
    vector<pair<string,string>> opts;

    for( auto opt: split( dsn, " " )) {
        size_t i = opt.find( "=" );

        string name = opt.substr( 0, i );
        string val = opt.substr( 1, -1 );

        if( name == "dbtype" )
            dbtype = val;

        opts.push_back( make_pair( name, val ));
    }

    if( !dbtype.empty() ) {
        dbi_conn conn = inst.conn_new(dbtype);

        for( auto o: opts )
            dbi_conn_set_option(conn, o.first.c_str(), o.second.c_str());

        return conn;
    }

    throw invalid_argument( "missing 'dbtype' entry in dsn string" );
}

class DBIConn : public Conn {
    dbi_conn _conn;

public:
    DBIConn( const string &dsn ) {
        _conn = parse_dsn( dsn );

        if( dbi_conn_connect(_conn) == 0 ) {
            const char *error_desc;
            dbi_conn_error(_conn, &error_desc );

            throw Error( error_desc);
        }
    }

    ~DBIConn() {
        dbi_conn_close(_conn);
    }

    bool begin() {
        return 0 == dbi_conn_transaction_begin(_conn);
    }

    bool commit() {
        return 0 == dbi_conn_transaction_commit(_conn);
    }

    bool rollback() {
        return 0 == dbi_conn_transaction_rollback(_conn);
    }

    bool savepoint( const string &name ) {
        return 0 == dbi_conn_savepoint(_conn, name.c_str());
    }

    bool savepoint_release( const string &name ) {
        return 0 == dbi_conn_release_savepoint( _conn, name.c_str());
    }

    bool savepoint_roleback( const string &name ) {
        return 0 == dbi_conn_rollback_to_savepoint( _conn, name.c_str());
    }

    string escape( const Value &val ) const;
    shared_ptr<Result> query( const string &stmt, resolve_fn fn ) const;
    long long update( const string &stmt, resolve_fn fn ) const;
};

string DBIConn::escape( const Value &val ) const
{
    if( val.is_null())
        return "NULL";

    stringstream os;

    os << val;

    if( val.is_type<long>() || val.is_type<double>() || val.is_type<bool>())
        return os.str();

    char *esc = NULL;
    size_t len = dbi_conn_quote_string_copy( _conn, os.str().c_str(), &esc );

    if( len > 0 ) {
        string res( esc, len );

        free( esc );

        return res;
    }

    const char *error_desc;
    dbi_conn_error(_conn, &error_desc );
    throw Error( error_desc, os.str());
}

shared_ptr<Result> DBIConn::query( const string &stmt, resolve_fn fn ) const
{
    stringstream os;

    parse_stmt(os, *this, stmt, fn );

    dbi_result r = dbi_conn_query(_conn, os.str().c_str());

    if( r != nullptr ) {
        shared_ptr<Result> res( make_shared<DBIResult>( r ));

        return res;
    }

    const char *error_desc;
    dbi_conn_error(_conn, &error_desc );
    throw Error( error_desc, os.str());
}

long long DBIConn::update( const string &stmt, resolve_fn fn ) const
{
    stringstream os;

    parse_stmt(os, *this, stmt, fn );

    dbi_result r = dbi_conn_query(_conn, os.str().c_str());

    if( r != nullptr ) {
        long long cnt = dbi_result_get_numrows_affected( r );

        dbi_result_free( r );

        return cnt;
    }

    const char *error_desc;
    dbi_conn_error(_conn, &error_desc );
    throw Error( error_desc, os.str());
}

// make a single connection
// XXX, Connection pool ?
shared_ptr<Conn> DB::connect( const string &dsn )
{
    shared_ptr<Conn> conn( make_shared<DBIConn>(dsn));

    return conn;
}