#include <string>
#include <vector>

#pragma once

namespace tools {
    using namespace std;

    // trim from end of string (right)
    string_view rtrim(string_view s);
    // trim from beginning of string (left)
    string_view ltrim(string_view s);
    // trim from both ends of string (left & right)
    string_view trim(string_view s);

    template<typename T> vector<T> split(const T & str, const T & delimiters) {
        vector<T> v;
        typename T::size_type start = 0;
        auto pos = str.find_first_of(delimiters, start);
        while(pos != T::npos) {
            if(pos != start) // ignore empty tokens
                v.emplace_back(str, start, pos - start);
            start = pos + 1;
            pos = str.find_first_of(delimiters, start);
        }
        if(start < str.length()) // ignore trailing delimiter
            v.emplace_back(str, start, str.length() - start); // add what's left of the string
        return v;
    }
}