/**
   This files try to make a command / execute model using
   C++11 new thread model.
*/
#pragma once

#include <functional>
#include <thread>
#include <list>
#include <tuple>
#include <atomic>
#include <mutex>
#include <chrono>
#include <condition_variable>

using namespace std;

// unpack templates (http://stackoverflow.com/questions/7858817/unpacking-a-tuple-to-call-a-matching-function-pointer)

template<typename ... Args> class CommandQueue {
    function<void (Args ...)> _fn;
    list<tuple<Args ...>> _queue;
    condition_variable _has_new, _ready;
    list<thread> _pool;     // The thread pool
    mutex _mutex;
    atomic<uint> _working;        // Number of working threads
    uint _max;
    bool _stop;

    template<int ...S> void callFunc( tuple<Args ...> &args ) {
        this->_working++;
        apply( _fn, args );
        this->_working--;
    }

    void check_pool() {
        if( _pool.size() < _max && !_queue.empty()) {
            unique_lock<mutex> l( _mutex );

            _pool.push_back( thread( [&] {
                while( ! _stop ) {
                    tuple<Args ...> args;
                    {
                        _ready.notify_one();
                        unique_lock<mutex> l( _mutex );

                        if( _queue.size() == 0 && ! _stop )
                            _has_new.wait( l );

                        if( _queue.size() > 0 ) {
                            args = _queue.front();

                            _queue.pop_front();
                        } else
                            continue;
                    }

                    this->callFunc( args );
                }
            }));
        }
    }

public:
    CommandQueue( function<void (Args ... )> fn, uint max = 10 ) : _fn( fn ), _working( 0 ), _max( max ), _stop( false ) {
        check_pool();
    }

    ~CommandQueue() {
        {
            unique_lock<mutex> l( _mutex );

            _stop = true;

            _has_new.notify_all();
        }

        for( auto &th: _pool )
            th.join();
    }

    size_t queue_current_size() const {
        return _queue.size();
    }

    size_t pool_size() const {
        return _pool.size();
    }

    uint working_count() const {
        return _working;
    }

    /*
        Wait for all workers to empty the queue
    */
    void wait() {
        while( _working ) {
            unique_lock<mutex> l( _mutex );

            _ready.wait( l );
        }
    }

    void operator()( Args... args ) {
        check_pool();

        unique_lock<mutex> l( _mutex );

        _queue.push_back( tuple<Args ...>( args... ));

        _has_new.notify_one();
    }
};

